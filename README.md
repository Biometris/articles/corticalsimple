#######################
# General information #
#######################

This code simulates microtubule dynamic instability for microtubules positioned along a single (y-) axis or on an x,y-plane. It is possible to simulate diffusion of free tubulin, with the microtubule growth speed v+ dependent on tubulin concentration. Multiple options exist for the nucleation of microtubules, including the possibility to explicitly simulate nucleation complexes. 
This code is developed by Bas Jacobs for the article: 'Nucleation complex behaviour is critical for cortical microtubule array homogeneity and patterning', available at bioRxiv: https://doi.org/10.1101/2022.04.05.487129. See the manuscript for details on parameters and options. If you use this code for your own purpose, please the manuscript. 

This code is written in python 2.7.

The latest version of this code can be downloaded from:
https://git.wur.nl/Biometris/articles/corticalsimple 

This code is released under the GNU Public Licence version 3 

##################################################
# CorticalSimple: 1D microtubule simulation code #
##################################################

Main simulation code:
CorticalSimple.py

Function for solving band matrices:
bandsolver.py

Required python libraries:
numpy
scipy
heapq
h5py

Typically, this simulation code will be called from a run script (see below). 

####################
# Plotting scripts #
####################

Various plotting functions in:
processOutput.py

Required python libraries:
matplotlib
numpy
scipy
h5py

Functions for producing various plot types in this script can be called by a run script (see below). 

######################
# Example run script #
######################

Run script used for FigS4.8 in Jacobs et al. 
ExampleRunScriptFigS4.8.py
(simulations with different nucleation complex insertion and nucleation rates)

This script contains example commands for running simulations (n independent replicates) as well as examples for plotting: snapshots of individual simulations (at given time points) and summary statistics for all replicates. 



