#!/usr/bin/python
from __future__ import division
import numpy as np
from scipy.linalg import solve_banded


def bandsolver(Mband,b,periodic = False, method = 'scipy', check_finite = True, overwrite_M = False, overwrite_b = False):
    M = np.copy(Mband)
    b = np.copy(b)
    if periodic:
        if method == 'scipy':
            # using the Sherman-Morrison formula
            a1 = M[0,0] # top right element
            b1 = M[1,0]
            cN = M[-1,-1] # bottom left element
            u = np.zeros(b.shape[0])
            u[0] = -b1
            u[-1] = cN
            M[1,-1] += a1*cN/b1
            M[1,0] *= 2
            M[0,0] = 0
            M[-1,-1] = 0

            y = solve_banded((1,1),M,b, check_finite = check_finite, overwrite_ab = overwrite_M, overwrite_b = overwrite_b)
            q = solve_banded((1,1),M,u, check_finite = check_finite, overwrite_ab = overwrite_M, overwrite_b = overwrite_b)

            vTq = q[0] - a1/b1 * q[-1]
            
            if len(b.shape) == 1:
                vTy = y[0] - a1/b1 * y[-1]
                b = y - vTy/(1+vTq)*q
            else:
                vTy = y[0,:] - a1/b1 * y[-1,:]
                b = y - np.outer(q,vTy)/(1+vTq)

        else:
            raise ValueError('Method %s not implemented'%str(method))
            
            
    else:
        if method == 'scipy':
            b = solve_banded((1,1),M,b, check_finite = check_finite, overwrite_ab = overwrite_M, overwrite_b = overwrite_b)

        else:
            raise ValueError('Method %s not implemented'%str(method))
            

    return b
