# -*- coding: utf-8 -*-
from __future__ import division
import sys
import numpy as np
from numpy import array
import matplotlib.pyplot as plt
from mpl_toolkits import axes_grid1
import os
import h5py
from matplotlib import ticker
from CorticalSimple import read_pars
from scipy import stats
from scipy.signal import periodogram


def plot_Tend(filename, fontsize = 50, ticks = True, outname = None, printTend = True, caption = None, cblabel = None, ntickscb = None, notext = False, textAbove = False, axlabel = True, frame = 'last', rotateLabels = False, nbinsLengthHist = 50, plotNCs = False, legend = True, burntime = 0, make_histogram = True, plotBandDensity = True, plotcircumference = False, plotMTdensityHistogram = False, nbins = 60, plotMovingAvg = False, window = 10, plotTubulin = True):
    # Load data
    if type(filename) == type(''):
        singlerun = True
        filenames = [filename]
    else:
        singlerun = False
        filenames = filename
    l_lengths = []
    l_y = []
    l_NCys = []
    l_tubulinconcentrations = []

    for filename in filenames:
        pardict = read_pars(filename)
        try:
            f = h5py.File(filename + '.hdf5','r')
            y = f['y'][:]
            t_list = f['t'][:]
            if frame == 'last':
                if 'useKataninRelease' in pardict and pardict['useKataninRelease']:
                    Gnlengths = f['Gnlengths_m' + str(len(t_list))][:]
                    Gtmlengths = f['Gtmlengths_m' + str(len(t_list))][:]
                    Snlengths = f['Snlengths_m' + str(len(t_list))][:]
                    Stmlengths = f['Stmlengths_m' + str(len(t_list))][:]
                    lengths = [Gnlengths, Snlengths, Gtmlengths, Stmlengths]
                    Gny = f['Gny_m' + str(len(t_list))][:]
                    Gtmy = f['Gtmy_m' + str(len(t_list))][:]
                    Sny = f['Sny_m' + str(len(t_list))][:]
                    Stmy = f['Stmy_m' + str(len(t_list))][:]
                    ys = [Gny, Sny, Gtmy, Stmy]
                else:
                    Glengths = f['Glengths_m' + str(len(t_list))][:]
                    Slengths = f['Slengths_m' + str(len(t_list))][:]
                    lengths = [Glengths, Slengths]
                    Gy = f['Gy_m' + str(len(t_list))][:]
                    Sy = f['Sy_m' + str(len(t_list))][:]
                    ys = [Gy, Sy]
                if 'tubulinMode' in pardict and pardict['tubulinMode'].startswith('Local'):
                    tubulinconcentrations = [f['tubulinconcentration1_m' + str(len(t_list))][:]]
                    if pardict['tubulinMode'] == 'Local2':
                        tubulinconcentrations += [f['tubulinconcentration2_m' + str(len(t_list))][:]]
                else:
                    tubulinconcentrations = None
                if plotNCs and pardict['MTdependentNucleation'] == 'NucleationComplexes':
                    NCys = f['NCy_m' + str(len(t_list))][:]
                    NCys2 = np.array([])
                    for jj in range(len(t_list)):
                        if t_list[jj] >= burntime:
                            NCys2 = np.append(NCys2,f['NCy_m' + str(jj+1)][:])
                else:
                    NCys = None
                if 'useMTjumpingNCs' in pardict and pardict['useMTjumpingNCs']:
                    Gxpos1 = f['Gxpos1_m' + str(len(t_list))][:]
                    Sxpos1 = f['Sxpos1_m' + str(len(t_list))][:]
                    xpos1s = [Gxpos1,Sxpos1]
                    if plotNCs:
                        NCxs = f['NCx_m' + str(len(t_list))][:]
                    else:
                        NCxs = None
                else:
                    xpos1s = None
                    NCxs = None

            else:
                Glengths = f['Glengths_m' + str(frame)][:]
                Slengths = f['Slengths_m' + str(frame)][:]
                Gy = f['Gy_m' + str(frame)][:]
                Sy = f['Sy_m' + str(frame)][:]
            f.close()
        except IOError:
            raise ValueError('File %s does not exist.'%(filename+'.hdf5'))
        

        # Time label
        if printTend:
            if frame == 'last':
                textlabel = '$t = %.0f$'%t_list[-1]
            else:
                textlabel = '$t = %.0f$'%t_list[frame-1]
            if caption:
                textlabel = caption + '\n' + textlabel
        else:
            textlabel = ''
            if caption:
                textlabel = caption

        if singlerun:
            # Output filename
            if outname == None:
                outname1 = filename
            else:
                outname1 = outname
        else:
            outname1 = filename

        l_lengths += [lengths]
        l_y += [ys]
        if plotNCs and pardict['MTdependentNucleation'] == 'NucleationComplexes':
            l_NCys += [NCys2]
        if 'tubulinMode' in pardict and pardict['tubulinMode'] == 'Local1':
            l_tubulinconcentrations += [tubulinconcentrations]
        else:
            l_tubulinconcentrations = None

        # Plot
        plot_solution(y, lengths, ys, pardict, textlabel, outname1, fontsize = fontsize, ticks = ticks, caption = caption, notext = notext, printTend = printTend, textAbove = textAbove, axlabel = axlabel, rotateLabels = rotateLabels, tubulinconcentrations = tubulinconcentrations, NCys = NCys, NCxs = NCxs, xpos1s = xpos1s, legend = legend, plotBandDensity = plotBandDensity, plotcircumference = plotcircumference, plotMTdensityHistogram = plotMTdensityHistogram, nbins = nbins, plotMovingAvg = plotMovingAvg, window = window, plotTubulin = plotTubulin)

    # Output filename
    if outname == None:
        outname = filename

    if make_histogram:
        plot_histogram(y, l_lengths, l_y, pardict, textlabel, outname, fontsize = fontsize, ticks = ticks, caption = caption, notext = notext, printTend = printTend, textAbove = textAbove, axlabel = axlabel, rotateLabels = rotateLabels, histfiles = not singlerun, nbins = nbinsLengthHist, tubulinconcentrations = l_tubulinconcentrations)
    if plotNCs and pardict['MTdependentNucleation'] == 'NucleationComplexes':
        plot_NCdistribution(l_NCys, pardict, textlabel, outname, fontsize = fontsize, ticks = ticks, caption = caption, notext = notext, printTend = printTend, textAbove = textAbove, axlabel = axlabel, rotateLabels = rotateLabels, legend = legend)


def gen_frames(filename, fontsize = 50, caption = None, printTend = True, cblabel = None, notext = False, textAbove = False, axlabel = True, ticks = True, maxLength = None, dmax = None, plotNCs = False, legend = True, plotMTdensityHistogram = False, nbins = 60):
    # Load data
    l_lengths = []
    l_y = []
    pardict = read_pars(filename)
    try:
        f = h5py.File(filename + '.hdf5','r')
        y = f['y'][:]
        t_list = f['t'][:]

        # setup ax limits
        Lmax = 0
        umin1 = 1e20
        umin2 = 1e20
        umax1 = 0
        umax2 = 0
        Dmax = 0
        for ii in range(len(t_list)):
            frame = ii + 1
            if maxLength == None or dmax == None:
                Glengths = f['Glengths_m' + str(frame)][:]
                Slengths = f['Slengths_m' + str(frame)][:]
                Gy = f['Gy_m' + str(frame)][:]
                Sy = f['Sy_m' + str(frame)][:]
            if maxLength != None:
                Lmax = maxLength
            else:
                if Glengths != []:
                    Lmax = max(Lmax,max(Glengths))
                if Slengths != []:
                    Lmax = max(Lmax,max(Slengths))
            if 'tubulinMode' in pardict and pardict['tubulinMode'].startswith('Local'):
                tubulinconcentrations = [f['tubulinconcentration1_m' + str(frame)][:]]
                umax1 = max(umax1,max(tubulinconcentrations[0]))
                umin1 = min(umin1,min(tubulinconcentrations[0]))
                if pardict['tubulinMode'] == 'Local2':
                    tubulinconcentrations += [f['tubulinconcentration2_m' + str(frame)][:]]
                    umax2 = max(umax2,max(tubulinconcentrations[1]))
                    umin2 = min(umin2,min(tubulinconcentrations[1]))
                tubulinbounds = [umin1,umin2,umax1,umax2]
            else:
                tubulinbounds = None
            # Use length densities from last frame to set ax limits
            if dmax != None:
                Dmax = dmax
            else:
                if 'useKataninRelease' in pardict and pardict['useKataninRelease']:
                    unsplit_lengths = np.append(np.append(np.append(Gnlengths, Snlengths), Gtmlengths), Stmlengths)
                    unsplit_ys = np.append(np.append(np.append(Gny, Sny), Gtmy), Stmy)
                else:
                    unsplit_lengths = np.append(Glengths, Slengths)
                    unsplit_ys = np.append(Gy,Sy)
                if plotMTdensityHistogram:
                    ybins, totallengths, barheights = getLengthDensities(pardict, unsplit_lengths, unsplit_ys, nbins = nbins)
                else:
                    ybins, totallengths, barheights = getLengthDensities(pardict, unsplit_lengths, unsplit_ys)
                Dmax = max(Dmax,max(totallengths))
        f.close()
    except IOError:
        raise ValueError('File %s does not exist.'%(filename+'.hdf5'))

    
    # Set up plot
    os.system('mkdir ' + filename)
    os.system('rm ' + filename + '/_tmp*.png')
    if 'useMTjumpingNCs' in pardict and pardict['useMTjumpingNCs']:
        overview = True
        os.system('mkdir ' + filename + '/overview/')
    else:
        overview = False
        
    files = []
    for ii in range(0, len(t_list)):
        frame = ii + 1
        f = h5py.File(filename + '.hdf5','r')
        Glengths = f['Glengths_m' + str(frame)][:]
        Slengths = f['Slengths_m' + str(frame)][:]
        Gy = f['Gy_m' + str(frame)][:]
        Sy = f['Sy_m' + str(frame)][:]
        lengths = [Glengths, Slengths]
        ys = [Gy, Sy]
        if 'tubulinMode' in pardict and pardict['tubulinMode'].startswith('Local'):
            tubulinconcentrations = [f['tubulinconcentration1_m' + str(frame)][:]]
            if pardict['tubulinMode'] == 'Local2':
                tubulinconcentrations += [f['tubulinconcentration2_m' + str(frame)][:]]
        else:
            tubulinconcentrations = None
        if plotNCs and pardict['MTdependentNucleation'] == 'NucleationComplexes':
            NCys = f['NCy_m' + str(frame)][:]
        else:
            NCys = None
        if 'useMTjumpingNCs' in pardict and pardict['useMTjumpingNCs']:
            Gxpos1 = f['Gxpos1_m' + str(frame)][:]
            Sxpos1 = f['Sxpos1_m' + str(frame)][:]
            xpos1s = [Gxpos1,Sxpos1]
            if plotNCs:
                NCxs = f['NCx_m' + str(frame)][:]
            else:
                NCxs = None
        else:
            xpos1s = None
            NCxs = None
        f.close()
        
        # Time label
        if printTend:
            textlabel = '$t = %.0f$'%t_list[ii]
            if caption:
                textlabel = caption + '\n' + textlabel
        else:
            textlabel = ''
            if caption:
                textlabel = caption
        fname = '_tmp%04d' % ii
        print 'Saving frame', fname
        plot_solution(y, lengths, ys, pardict, textlabel, filename+'/'+fname, fontsize = fontsize, ticks = ticks, caption = caption, notext = notext, printTend = printTend, textAbove = textAbove, axlabel = axlabel, Lmax = Lmax, dmax = Dmax, tubulinconcentrations = tubulinconcentrations, tubulinbounds = tubulinbounds, NCys = NCys, NCxs = NCxs, xpos1s = xpos1s, legend = legend, plotMTdensityHistogram = plotMTdensityHistogram, nbins = nbins)
        files.append(fname)

        if overview:
            fname = fname + '_overview'
            os.system('mv ' + filename + '/' + fname + '.png ' + filename + '/overview/')
            files.append('/overview/' + fname)

    return files

def movie_solution(filename, filetype = 'avi', fontsize = 50, cleanup = True, caption = None, printTend = True, textAbove = True, outname = None, Lmax = None, ticks = True, figSizeFactor = 1, bitrateFactor = 1, dmax = None, plotNCs = False, legend = True, plotMTdensityHistogram = False, notext = False):
    """Make movie of all stored data points in a data file.
filename: name of datafile (without extension)
variable: number of the variable to plot (default = 0; active form of first ROP)
filetype: filetype of movie to be made (either 'avi' or 'mpg')
cmap: Specify matplotlib colormap to use for the movie
fontsize: Specify the fontsize of the labels and axis numbers
cleanup: If False, temporary image files corresponding to the frames of the movie will not be removed after the movie has been generated"""
    files = gen_frames(filename, fontsize, caption = caption, printTend = printTend, textAbove = textAbove, maxLength = Lmax, ticks = ticks, dmax = dmax, plotNCs = plotNCs, legend = legend, plotMTdensityHistogram = plotMTdensityHistogram, notext = notext)
    fps = 15
    if outname == None:
        outname = filename
    
    hfile = os.popen('identify -format "%h" ' + filename + '/_tmp0000.png')
    wfile = os.popen('identify -format "%w" ' + filename + '/_tmp0000.png')
    h = int(int(hfile.read())*figSizeFactor)
    w = int(int(wfile.read())*figSizeFactor)
    hfile.close()
    wfile.close()
    bitrate = int((60 * 25 * w * h / 256) * bitrateFactor)

    # resize figures if necessary
    if figSizeFactor != 1:
        for fname in files:
            command = r"convert " + filename + r'/' + fname + r".png -resize $(echo `identify -format '%w' " + filename + '/' + fname + ".png`*" + str(figSizeFactor) + r"|bc) " + filename + '/' + fname + '.png'
            os.system(command)

    # use mencoder to make movie
    overview = False
    genMovie(filename, outname, filetype, bitrate, fps)
    if files[1].endswith('overview'):
        overview = True
        genMovie(filename+'/overview/', outname+'_overview', filetype, bitrate, fps)

    if cleanup:
        # cleanup
        for fname in files:
            os.remove(filename+'/'+fname+'.png')
        if overview:
            os.rmdir(filename+'/overview/')
        os.rmdir(filename)

def genMovie(filename, outname, filetype, bitrate, fps):
    if filetype == 'mpg':
        os.system("mencoder 'mf://" + filename + "/_tmp*.png' -mf type=png:fps="+str(fps)+" -ovc lavc -lavcopts vcodec=wmv2 -oac copy -o "+outname+".mpg")
    elif filetype == 'avi':
        os.system("mencoder 'mf://" + filename + "/_tmp*.png' -mf type=png:fps="+str(fps)+" -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=" + str(bitrate) + ":vhq -oac copy -o "+outname+".avi")
    elif filetype == 'avi_old':
        os.system("mencoder 'mf://" + filename + "/_tmp*.png' -mf type=png:fps="+str(fps)+" -ovc lavc -lavcopts vcodec=msmpeg4v2:vbitrate=3000:vhq -oac copy -o "+outname+".avi")

def getLengthDensities(pardict, lengths, ys, nbins = None):
    H = pardict['H']
    W = pardict['W']
    if type(nbins) == type(None):
        nBands = pardict['nBands']
        bandwidth = pardict['bandwidth']
        totalBandwidth = nBands*bandwidth
        totalGapwidth = H-totalBandwidth
        gapwidth = totalGapwidth/nBands
        bandstartpos = gapwidth/2
    else:
        nBands = int(round(nbins/2))
        bandwidth = H/nbins
        totalBandwidth = nBands*bandwidth
        totalGapwidth = H-totalBandwidth
        gapwidth = totalGapwidth/nBands
        bandstartpos = gapwidth
        
    
    bins = [0,bandstartpos]
    inband = True
    pos = bandstartpos
    while pos < H:
        if inband:
            pos += bandwidth
            inband = False
        else:
            pos += gapwidth
            inband = True
        bins += [pos]
    bins[-1] = H
    bins = np.array(bins)

    totallengths = np.zeros(len(bins)-1)
    ybins = np.zeros(len(bins)-1)
    barheights = []
    for jj in range(1,len(bins)):
        binstart = bins[jj-1]
        binend = bins[jj]
        binsize = binend - binstart
        if type(nbins) == type(None):
            barheights += [0.9*binsize]
            if jj == 1:
                ybins[jj-1] = binstart + binsize/2 - binsize*0.05
            elif jj == len(bins)-1:
                ybins[jj-1] = binstart + binsize/2 + binsize*0.05
            else:
                ybins[jj-1] = binstart + binsize/2
        else:
            ybins[jj-1] = binstart + binsize/2
            barheights += [binsize]
        for ii in range(len(ys)):
            ypos = ys[ii]
            if ypos < binend and ypos > binstart:
                totallengths[jj-1] += lengths[ii]
        totallengths[jj-1] /= binsize
    totallengths /= W
    return ybins, totallengths, barheights

def add_colorbar(im, aspect=20, pad_fraction=0.5, **kwargs):
    """Add a vertical color bar to an image plot."""
    divider = axes_grid1.make_axes_locatable(im.axes)
    width = axes_grid1.axes_size.AxesY(im.axes, aspect=1./aspect)
    pad = axes_grid1.axes_size.Fraction(pad_fraction, width)
    current_ax = plt.gca()
    cax = divider.append_axes("right", size=width, pad=pad)
##    cax = divider.append_axes("left", size=width, pad=pad)
    plt.sca(current_ax)
    return im.axes.figure.colorbar(im, cax=cax, **kwargs)

def plot_movingAverage(filename, window = 10, fontsize = 50, ticks = True, outname = None, printTend = True, caption = None, cblabel = None, ntickscb = None, notext = False, textAbove = False, axlabel = True, frame = 'last', rotateLabels = False, nbinsLengthHist = 50, plotNCs = False, legend = True, burntime = 0, make_histogram = True, plotBandDensity = True, plotcircumference = False, plotMTdensityHistogram = False, nbins = 60, plot_ylabels = True, newplot = True):
    # Load data
    l_lengths = []
    l_y = []
    pardict = read_pars(filename)
    try:
        f = h5py.File(filename + '.hdf5','r')
        y = f['y'][:]
        t_list = f['t'][:]

        # setup ax limits
        Dmax = 0
        for ii in range(len(t_list)):
            frame = ii + 1
            Glengths = f['Glengths_m' + str(frame)][:]
            Slengths = f['Slengths_m' + str(frame)][:]
            Gy = f['Gy_m' + str(frame)][:]
            Sy = f['Sy_m' + str(frame)][:]

            unsplit_lengths = np.append(Glengths, Slengths)
            unsplit_ys = np.append(Gy,Sy)
            ybins, totallengths, barheights = getLengthDensities(pardict, unsplit_lengths, unsplit_ys)
            Dmax = max(Dmax,max(totallengths))
        f.close()
    except IOError:
        raise ValueError('File %s does not exist.'%(filename+'.hdf5'))

    if newplot:
        plt.gcf().clear()
        params = {'legend.fontsize': 'xx-large',
            'axes.labelsize': 'xx-large',
             'axes.titlesize':'xx-large',
             'xtick.labelsize':'xx-large',
             'ytick.labelsize':'xx-large'}
        plt.rcParams.update(params)
    if 'tubulinMode' in pardict:
        tubulinMode = pardict['tubulinMode']
    else:
        tubulinMode = 'NoTubulin'
    if 'useMTjumpingNCs' in pardict and pardict['useMTjumpingNCs'] and pardict['MTdependentNucleation'] == 'NucleationComplexes':
        useMTjumpingNCs = pardict['useMTjumpingNCs']
    else:
        useMTjumpingNCs = False

    if newplot:
        fig1 = plt.figure()
        ax1 = fig1.gca()
    linewidth = 4

    sds = []
    densities = []
    for ii in range(0, len(t_list)):
        f = h5py.File(filename + '.hdf5','r')
        frame = ii + 1
        Glengths = f['Glengths_m' + str(frame)][:]
        Slengths = f['Slengths_m' + str(frame)][:]
        Gy = f['Gy_m' + str(frame)][:]
        Sy = f['Sy_m' + str(frame)][:]
        lengths = np.append(Glengths, Slengths)
        ys = np.append(Gy,Sy)
        f.close()

        
        if plotMTdensityHistogram:
            # find total length densities in 1 um intervals
            ybins, totallengths, barheights = getLengthDensities(pardict, lengths, ys, nbins = nbins)
            binsize = ybins[1]-ybins[0]
            barheights = binsize
        else:
            # find total lengths in bands and gaps
            ybins, totallengths, barheights = getLengthDensities(pardict, lengths, ys)
            
        if ii >= window:
            average = np.array(densities).mean(0)
            if plotMTdensityHistogram:
                sds += [average.std()]
            else:
                sds += [average[range(1,len(average),2)].std()]
            densities[ii%window] = totallengths
        else:
            densities += [totallengths]

    average = np.array(densities).mean(0)
    if plotMTdensityHistogram:
        sds += [average.std()]
    else:
        sds += [average[range(1,len(average),2)].std()]

    if not newplot:
        return ybins,average,barheights

    else:
        ax1.barh(ybins,average,height=barheights)

        if outname == None:
            outname = filename

        ax1.set_ylim(bottom=0,top=pardict['H'])
        ax1.set_xlim(left=0)
        ax1.set_xlabel('Time-averaged density\n($\mu m/\mu m^2$)', fontsize = fontsize)
        if plot_ylabels:
            ax1.set_ylabel('$y$ ($\mu m$)', fontsize = fontsize)
        if not notext and ticks:
            ax1.tick_params(axis='x', which='major', labelsize=fontsize)
            ax1.tick_params(axis='y', which='major', labelsize=fontsize, labelrotation = 90)
        else:
            ax1.set_xticks([])
            ax1.set_yticks([])
        if not plot_ylabels:
            ax1.set_yticks([])

        default_size = fig1.get_size_inches()
        fig1.set_size_inches( (default_size[0]*2, default_size[1]*3) )
        fig1.tight_layout(pad = 4, w_pad=0)

        fig1.savefig(outname + '_timeAvg')

        plt.close('all')
    
            



def plot_solution(y, lengths, ys, pardict, textlabel, filename, fontsize = 50, ticks = True, caption = False, printTend = True, notext = False, textAbove = False, axlabel = True, kymograph = False, xlab = None, rotateLabels = False, Lmax = None, dmax = None, tubulinconcentrations = None, tubulinbounds = None, NCys = None, NCxs = None, xpos1s = None, legend = True, plotBandDensity = True, plotcircumference = False, plotMTdensityHistogram = False, nbins = 60, plotMovingAvg = False, window = 10, plotTubulin = True):
    plt.gcf().clear()
    params = {'legend.fontsize': 'xx-large',
        'axes.labelsize': 'xx-large',
         'axes.titlesize':'xx-large',
         'xtick.labelsize':'xx-large',
         'ytick.labelsize':'xx-large'}
    plt.rcParams.update(params)
    if 'tubulinMode' in pardict:
        tubulinMode = pardict['tubulinMode']
    else:
        tubulinMode = 'NoTubulin'
    if 'useMTjumpingNCs' in pardict and pardict['useMTjumpingNCs'] and pardict['MTdependentNucleation'] == 'NucleationComplexes':
        useMTjumpingNCs = pardict['useMTjumpingNCs']
    else:
        useMTjumpingNCs = False
    if tubulinMode.startswith('Local') and plotTubulin:
        if plotBandDensity or plotMovingAvg:
            fig, axs = plt.subplots(1,3)
            ax1, ax2, ax3 = axs
        else:
            fig, axs = plt.subplots(1,2)
            ax1, ax3 = axs
    else:
        if plotBandDensity or plotMovingAvg:
            fig, axs = plt.subplots(1,2)
            ax1, ax2 = axs
        else:
            fig = plt.figure()
            ax1 = fig.gca()
            axs = (ax1,)
    if useMTjumpingNCs:
        fig2 = plt.figure()
        ax3 = fig2.gca()
        axs = [ax for ax in axs] + [ax3]
    linewidth = 4

    # plot bands vs gaps if applicable
    if 'useBands' in pardict and pardict['useBands'] and 'nBands' in pardict and pardict['nBands'] > 0:
        nBands = pardict['nBands']
        bandwidth = pardict['bandwidth']
        H = pardict['H']
        totalBandwidth = nBands*bandwidth
        totalGapwidth = H-totalBandwidth
        gapwidth = totalGapwidth/nBands
        period = bandwidth + gapwidth
        bandstartpos = gapwidth/2
        bandendpos = bandstartpos + bandwidth
        nextStartPos = bandstartpos
        nextEndPos = bandendpos
        while nextStartPos < H:
            y1 = nextStartPos
            y2 = nextEndPos
            for ax in axs:
                ax.axhspan(y1,y2,color='grey',fill=True,alpha = 0.2)
            nextStartPos += period
            nextEndPos += period

    # plot MTs as horizontal lines
    if 'useKataninRelease' in pardict and pardict['useKataninRelease']:
        Gny, Sny, Gtmy, Stmy = ys
        Gnlengths, Snlengths, Gtmlengths, Stmlengths = lengths
        ax1.hlines(Gny,0,Gnlengths,colors = 'b', label = 'Growing MTs', zorder = 1)
        ax1.hlines(Sny,0,Snlengths,colors = 'r', label = 'Shrinking MTs', zorder = 2)
        ax1.hlines(Gtmy,0,Gtmlengths,colors = 'b', label = 'Growing MTs', zorder = 3)
        ax1.hlines(Stmy,0,Stmlengths,colors = 'r', label = 'Shrinking MTs', zorder = 4)
        lengths = np.append(np.append(np.append(Gnlengths, Snlengths), Gtmlengths), Stmlengths)
        ys = np.append(np.append(np.append(Gny, Sny), Gtmy), Stmy)
    else:
        Gy, Sy = ys
        Glengths, Slengths = lengths
        ax1.hlines(Gy,0,Glengths,colors = 'b', label = 'Growing MTs', zorder = 1)
        ax1.hlines(Sy,0,Slengths,colors = 'r', label = 'Shrinking MTs', zorder = 2)
        lengths = np.append(Glengths, Slengths)
        ys = np.append(Gy,Sy)

    # plot line for circumference
    if plotcircumference:
        ax1.axvline(x=pardict['W'],color = 'black', linestyle = '--')

    # plot NCs
    if type(NCys) != type(None):
        ax1.scatter(np.zeros(len(NCys)),NCys, color = 'g', label = 'Nucleation Complexes', zorder = 10)

    # plot MT length density
    if plotMovingAvg:
        ybins,totallengths,barheights = plot_movingAverage(filename, window = window, fontsize = 50, ticks = True, outname = None, printTend = True, caption = None, cblabel = None, ntickscb = None, notext = False, textAbove = False, axlabel = True, frame = 'last', rotateLabels = False, nbinsLengthHist = 50, plotNCs = False, legend = True, burntime = 0, make_histogram = True, plotBandDensity = True, plotcircumference = False, plotMTdensityHistogram = plotMTdensityHistogram, nbins = nbins, plot_ylabels = True, newplot = False)
        ax2.barh(ybins,totallengths,height=barheights)
    else:
        if plotBandDensity:
            if plotMTdensityHistogram:
                # plot total length densities in 1 um intervals
                ybins, totallengths, barheights = getLengthDensities(pardict, lengths, ys, nbins = nbins)
                binsize = ybins[1]-ybins[0]
                ax2.barh(ybins,totallengths,height=binsize)
            elif not pardict['useBands'] and not (pardict['nBands'] > 0 and pardict['bandwidth'] > 0):
                counts,bins = np.histogram(ys,nbins,(0,pardict['H']),weights = lengths, density = True)
                binsize = bins[1]-bins[0]
                ybins = bins[:-1]+(binsize)/2
            ##    counts = counts / binsize
                ax2.barh(ybins,counts,height=binsize)
            else:
                # plot total lengths in bands and gaps
                ybins, totallengths, barheights = getLengthDensities(pardict, lengths, ys)
                ax2.barh(ybins,totallengths,height=barheights)
        

    # plot Tubulin concentrations
    y = y[0]
    if tubulinMode == 'Local1' and plotTubulin:
        ax3.plot(tubulinconcentrations[0],y, label = 'Tubulin', linewidth = linewidth)
    elif tubulinMode == 'Local2' and plotTubulin:
        ax4 = ax3
        ax3 = ax4.twiny()
        if plotBandDensity or plotMovingAvg:
            axs = [ax1, ax2, ax3, ax4]
        else:
            axs = [ax1, ax3, ax4]
        ax3.plot([],[],label = 'GTP-tubulin',linewidth = linewidth, zorder = 99)
        ax3.plot(tubulinconcentrations[1],y, color = 'orange', label = 'GDP-tubulin', linewidth = linewidth, zorder = 7)
        ax4.plot(tubulinconcentrations[0],y, label = 'GTP-tubulin', linewidth = linewidth, zorder = 6)
        
        

    # plot impression of MT and NC locations
    if useMTjumpingNCs:
        Gxpos1s, Sxpos1s = xpos1s
        ax3.hlines(Gy,Gxpos1s,Gxpos1s+Glengths,colors = 'b', label = 'Growing MTs', zorder = 1)
        ax3.hlines(Sy,Sxpos1s,Sxpos1s+Slengths,colors = 'r', label = 'Shrinking MTs', zorder = 2)
        ax3.hlines(Gy,0,Gxpos1s+Glengths-pardict['W'],colors = 'b', zorder = 3)
        ax3.hlines(Sy,0,Sxpos1s+Slengths-pardict['W'],colors = 'r', zorder = 4)
        if type(NCxs) != type(None) and type(NCys) != type(None):
            ax3.scatter(NCxs,NCys, color = 'g', label = 'Nucleation Complexes', zorder = 10)
        

    for ax in axs:
        ax.set_ylim(bottom=0,top=pardict['H'])
    if Lmax:
        ax1.set_xlim(left=0,right=Lmax)
    else:
        ax1.set_xlim(left=0)
    if plotBandDensity or plotMovingAvg:
        if dmax:
            ax2.set_xlim(left=0,right=dmax)
        else:
            ax2.set_xlim(left=0)
    if tubulinMode.startswith('Local') and tubulinbounds != None and plotTubulin:
        umin1,umin2,umax1,umax2 = tubulinbounds
        if tubulinMode == 'Local1':
            ax3.set_xlim(left=umin1,right=umax1)
        if tubulinMode == 'Local2':
            ax4.set_xlim(left=umin1,right=umax1)
            ax3.set_xlim(left=umin2,right=umax2)
    elif useMTjumpingNCs:
        ax3.set_xlim(left = 0, right = pardict['W'])
        ax3.set_aspect('equal', adjustable='box')

    if legend:
        ax1.legend(fontsize=fontsize/2, loc = 'upper right').set_zorder(100)
        if (tubulinMode.startswith('Local') and plotTubulin) or useMTjumpingNCs:
            ax3.legend(fontsize=fontsize/2, loc = 'upper right').set_zorder(100)
    
    if printTend and caption:
        plt.title(textlabel, fontsize = fontsize, y = 1.07, x = 0.8)
    elif (printTend and textAbove) or caption:
        plt.suptitle(textlabel, fontsize = fontsize, y = 0.99, x = 0.53)
    elif printTend:
        ax1.text(0.04, 1-0.07*fontsize/50, textlabel, transform=ax.transAxes, fontsize = fontsize)
    if axlabel and not notext:
        ax1.set_ylabel('$y$ ($\mu m$)', fontsize = fontsize)
        ax1.set_xlabel('Microtubule length\n($\mu m$)', fontsize = fontsize)
        if plotBandDensity or plotMovingAvg:
            if plotMovingAvg:
                ax2.set_xlabel('Time-averaged density\n($\mu m/\mu m^2$)', fontsize = fontsize)
            elif pardict['useBands'] or plotMTdensityHistogram:
                ax2.set_xlabel('MT density\n($\mu m/\mu m^2$)', fontsize = fontsize)
            else:
                ax2.set_xlabel('Length density', fontsize = fontsize)
        if tubulinMode == 'Local1' and plotTubulin:
            ax3.set_xlabel('[Tubulin]\n($\mu m/\mu m^2$)', fontsize = fontsize)
        elif tubulinMode == 'Local2' and plotTubulin:
            ax4.set_xlabel('[GTP-tubulin]\n($\mu m/\mu m^2$)', fontsize = fontsize)
            ax3.set_xlabel('[GDP-tubulin]', fontsize = fontsize)
        elif useMTjumpingNCs:
            ax3.set_xlabel('$x$ ($\mu m$)', fontsize = fontsize)
            ax3.set_ylabel('$y$ ($\mu m$)', fontsize = fontsize)
    for ax in axs:
        if not notext and ticks:
            ax.tick_params(axis='x', which='major', labelsize=fontsize)
            ax.tick_params(axis='y', which='major', labelsize=fontsize, labelrotation = 90)
        else:
            ax.set_xticks([])
            ax.set_yticks([])
    if plotBandDensity or plotMovingAvg:
        ax2.set_yticks([])
    if tubulinMode.startswith('Local') and plotTubulin:
        ax3.set_yticks([])
    default_size = fig.get_size_inches()
    widthfactor = len(axs)
    if (tubulinMode == 'Local2' and plotTubulin) or useMTjumpingNCs:
        widthfactor -= 1
    fig.set_size_inches( (default_size[0]*(0.5+widthfactor*1.25), default_size[1]*3) )
    fig.tight_layout(pad = 4, w_pad=0)
    if useMTjumpingNCs:
        default_size = fig2.get_size_inches()
        fig2.set_size_inches( (default_size[0]*2, default_size[1]*3) )
        fig2.tight_layout(pad = 4, w_pad=0)
    

    fig.savefig(filename + '.png')
    if useMTjumpingNCs:
        fig2.savefig(filename + '_overview.png')
    plt.close('all')

def plot_NCdistribution(NCys, pardict, textlabel, filename, fontsize = 50, ticks = True, caption = False, printTend = True, notext = False, textAbove = False, axlabel = True, kymograph = False, xlab = None, rotateLabels = False, Lmax = None, dmax = None, legend = True):
    plt.gcf().clear()
    params = {'legend.fontsize': 'xx-large',
        'axes.labelsize': 'xx-large',
         'axes.titlesize':'xx-large',
         'xtick.labelsize':'xx-large',
         'ytick.labelsize':'xx-large'}
    plt.rcParams.update(params)
    fig = plt.figure()
    ax = fig.gca()
    nbins = 300
    linewidth = 4

    # plot bands vs gaps if applicable
    if 'useBands' in pardict and pardict['useBands'] and 'nBands' in pardict and pardict['nBands'] > 0:
        nBands = pardict['nBands']
        bandwidth = pardict['bandwidth']
        H = pardict['H']
        totalBandwidth = nBands*bandwidth
        totalGapwidth = H-totalBandwidth
        gapwidth = totalGapwidth/nBands
        period = bandwidth + gapwidth
        bandstartpos = gapwidth/2
        bandendpos = bandstartpos + bandwidth
        nextStartPos = bandstartpos
        nextEndPos = bandendpos
        while nextStartPos < H:
            y1 = nextStartPos
            y2 = nextEndPos
            ax.axhspan(y1,y2,color='grey',fill=True,alpha = 0.2)
            nextStartPos += period
            nextEndPos += period

    # gather NCy data
    data = np.array([])
    for NCy in NCys:
        data = np.append(data,NCy)
    
    # plot MT length density
    counts,bins = np.histogram(data,nbins,(0,pardict['H']), density = True)
    binsize = bins[1]-bins[0]
    ybins = bins[:-1]+(binsize)/2
##    counts = counts / binsize
##    ax.barh(ybins,counts,height=binsize)
    ax.plot(counts,ybins, color = 'g', linewidth = linewidth)

    ax.set_ylim(bottom=0,top=pardict['H'])
    ax.set_xlim(left=0)
##    if legend:
##        ax.legend(fontsize=fontsize/2, loc = 'upper right').set_zorder(100)
##        if tubulinMode.startswith('Local') or useMTjumpingNCs:
##            ax3.legend(fontsize=fontsize/2, loc = 'upper right').set_zorder(100)
##    
    if printTend and caption:
        plt.title(textlabel, fontsize = fontsize, y = 1.07, x = 0.8)
    elif (printTend and textAbove) or caption:
        plt.suptitle(textlabel, fontsize = fontsize, y = 0.99, x = 0.53)
    elif printTend:
        ax1.text(0.04, 1-0.07*fontsize/50, textlabel, transform=ax.transAxes, fontsize = fontsize)
    if axlabel and not notext:
        ax.set_ylabel('$y$', fontsize = fontsize)
        ax.set_xlabel('NC density', fontsize = fontsize)
        
    if not notext and ticks:
        ax.tick_params(axis='x', which='major', labelsize=fontsize)
        ax.tick_params(axis='y', which='major', labelsize=fontsize, labelrotation = 90)
    else:
        ax.set_xticks([])
        ax.set_yticks([])
    default_size = fig.get_size_inches()
    fig.set_size_inches( (default_size[0]*2, default_size[1]*3) )
    plt.tight_layout(pad = 4, w_pad=0)

    fig.savefig(filename + '_NCdensity.png')
    plt.close('all')

def plot_histogram(y, lengths, ys, pardict, textlabel, filename, fontsize = 50, ticks = True, caption = False, printTend = True, notext = False, textAbove = False, axlabel = True, kymograph = False, xlab = None, rotateLabels = False, histfiles = False, nbins = 50, tubulinconcentrations = None):
    plt.gcf().clear()
    params = {'legend.fontsize': 'xx-large',
        'axes.labelsize': 'xx-large',
         'axes.titlesize':'xx-large',
         'xtick.labelsize':'xx-large',
         'ytick.labelsize':'xx-large'}
    plt.rcParams.update(params)
    fig = plt.figure()
    ax = fig.gca()

    if 'useKataninRelease' in pardict and pardict['useKataninRelease']:
        labels = ['Sim +', 'Sim -', 'Sim + tm', 'Sim - tm']
        colors = ['b', 'r', 'c', 'm']
    else:
        labels = ['Growing MTs', 'Shrinking MTs']
        colors = ['b', 'r']

    xmax = 0
    for jj in range(len(lengths[0])):
        for ii in range(len(lengths)):
            counts,bins = np.histogram(lengths[ii][jj],nbins,(0,lengths[ii][jj].max()))
            binsize = bins[1]-bins[0]
            l = bins[:-1]+(binsize)/2
            counts = counts / binsize
            ax.plot(l,counts, color = colors[jj])
            xmax = max(xmax,l.max())
        ax.plot([], [], label=labels[jj], color = colors[jj])

    # theoretical curves
    if (pardict['MTdependentNucleation'] == 'Simple' and pardict['useBands']) or ('tubulinMode' in pardict and pardict['tubulinMode'] != 'NoTubulin'):
        labelBase = 'Theory$^*$ '
    else:
        labelBase = 'Theory '

    if 'tubulinMode' in pardict and pardict['tubulinMode'] == 'Global':
        # adjust vplus to its average final effective value
        vplusses = []
        for length in lengths:
            Lt = sum([sum(l) for l in length])
            vplusses += [pardict['vplus']*(1-Lt/pardict['Lmax'])]
        pardict['vplus'] = sum(vplusses)/len(vplusses)

    if 'tubulinMode' in pardict and pardict['tubulinMode'] == 'Local1':
        # adjust vplus to its average final effective value
        vplus0 = pardict['vplus']
        A = pardict['H']*pardict['W']
        Lmax = pardict['Lmax']
        vplusses = []
        for rep in range(len(tubulinconcentrations)):
            cTs = tubulinconcentrations[rep]
            cT1 = cTs[0]
            vplusses += [vplus0*A/Lmax*cT1[ii] for ii in range(len(cT1))]
        pardict['vplus'] = sum(vplusses)/len(vplusses)
        
    if (pardict['MTdependentNucleation'] == 'Simple' or not pardict['useBands']) and not pardict['useKataninRelease']:
        pardict['vmin'] += pardict['vtm']
        pardict['vplus'] -= pardict['vtm']
        lam = (pardict['rcat']*pardict['vmin']-pardict['rres']*pardict['vplus'])/(pardict['vmin']*pardict['vplus'])
        l = np.linspace(0,xmax*2,1000)
        countsT = pardict['rn']*pardict['W']*pardict['H']/pardict['vplus']*np.exp(-lam*l)
        ax.plot(l,countsT, color = 'c', label = labelBase + '+',linewidth = 3)

        l = np.linspace(0,xmax*2,1000)
        countsT = pardict['rn']*pardict['W']*pardict['H']/pardict['vmin']*np.exp(-lam*l)
        ax.plot(l,countsT, color = 'm', label = labelBase + '-',linewidth = 3)

    elif pardict['useKataninRelease']:
        vp = pardict['vplus']
        vm = pardict['vmin']
        vtm = pardict['vtm']
        rc = pardict['rcat']
        rr = pardict['rres']
        rk = pardict['rrel']
        rn = pardict['rn']*pardict['H']*pardict['W']
        A = np.array([[(-rk-rc)/vp, rr/vp, 0, 0], [-rc/vm, (rk+rr)/vm, 0, 0], [rk/(vp-vtm), 0, -rc/(vp-vtm), rr/(vp-vtm)], [0, -rk/(vm+vtm), -rc/(vm+vtm), rr/(vm+vtm)]])
        lambdas,evecs = np.linalg.eig(A)
        relevant = (lambdas<=-1e-10).nonzero()[0]
        if len(relevant) != 2:
            raise ValueError('Unexpected number of negative eigenvalues for katanin release system')
        lambdas = lambdas[relevant]
        evecs = evecs[:,relevant]
        B = np.array([[evecs[0,0],evecs[0,1]],[evecs[2,0],evecs[2,1]]])
        v = np.array([rn/vp,0])
        coeffs = np.linalg.solve(B,v)

        l = np.linspace(0,xmax*2,1000)
        labels = ['+','-','+ tm', '- tm']
        for ii in range(4):
            countsT = coeffs[0]*evecs[ii,0]*np.exp(lambdas[0]*l) + coeffs[1]*evecs[ii,1]*np.exp(lambdas[1]*l)
            ax.plot(l,countsT, color = colors[ii], label = labelBase + labels[ii],linewidth = 3)

    elif pardict['useBands']:
        pardict['vmin'] += pardict['vtm']
        pardict['vplus'] -= pardict['vtm']
        rcat = pardict['rcat']
        lam1 = (rcat*pardict['vmin']-pardict['rres']*pardict['vplus'])/(pardict['vmin']*pardict['vplus'])
        rcat = pardict['rcatGaps']
        lam2 = (rcat*pardict['vmin']-pardict['rres']*pardict['vplus'])/(pardict['vmin']*pardict['vplus'])

        nBands = pardict['nBands']
        bandwidth = pardict['bandwidth']
        H = pardict['H']
        totalBandwidth = nBands*bandwidth
        totalGapwidth = H-totalBandwidth
        gapwidth = totalGapwidth/nBands
        period = bandwidth + gapwidth
        
        l = np.linspace(0,xmax*2,1000)
        countsT = bandwidth/period * pardict['rn']*pardict['W']*pardict['H']/pardict['vplus']*np.exp(-lam1*l) + gapwidth/period * pardict['rn']*pardict['W']*pardict['H']/pardict['vplus']*np.exp(-lam2*l)
        ax.plot(l,countsT, color = 'c', label = labelBase + '+',linewidth = 3)

        l = np.linspace(0,xmax*2,1000)
        countsT = bandwidth/period * pardict['rn']*pardict['W']*pardict['H']/pardict['vmin']*np.exp(-lam1*l) + gapwidth/period * pardict['rn']*pardict['W']*pardict['H']/pardict['vmin']*np.exp(-lam2*l)
        ax.plot(l,countsT, color = 'm', label = labelBase + '-',linewidth = 3)
        
    
    
    plt.yscale('log')
    ax.set_ylim(bottom=0.1)
    ax.set_xlim(left=0,right=xmax*1.1)
    plt.legend(fontsize=fontsize/2, loc = 'upper right')
    
    if printTend and caption:
        plt.title(textlabel, fontsize = fontsize, y = 1.07, x = 0.8)
    elif (printTend and textAbove) or caption:
        plt.title(textlabel, fontsize = fontsize, y = 1.02, x = 0.5)
    elif printTend:
        ax.text(0.04, 1-0.07*fontsize/50, textlabel, transform=ax.transAxes, fontsize = fontsize)
    if axlabel and not notext:
        plt.ylabel('$Frequency$', fontsize = fontsize)
        plt.xlabel('$Length$', fontsize = fontsize)
    if not notext and ticks:
        ax.tick_params(axis='x', which='major', labelsize=fontsize)
        ax.tick_params(axis='y', which='major', labelsize=fontsize, labelrotation = 90)
    else:
        ax.set_xticks([])
        ax.set_yticks([])
    default_size = fig.get_size_inches()
    fig.set_size_inches( (default_size[0]*2, default_size[1]*3) )
    plt.tight_layout()

    plt.savefig(filename + '_H.png')
    plt.close()

def plotBandLengthSD(*args,**kwargs):
    plotSummaryQuantities(*args,**kwargs)

def equilibriumGapBandRatioIso(pardict,t,vplusIso = None):
    if vplusIso != None and pardict['tubulinMode'] != 'NoTubulin':
        vplus = vplusIso
    else:
        vplus = pardict['vplus']
    vmin = pardict['vmin']
    vtm = pardict['vtm']
    rres = pardict['rres']
    rcat = pardict['rcat']
    rcatGap = pardict['rcatGaps']
    if 'parameterSwitchTimes' in pardict:
        switchtimes = pardict['parameterSwitchTimes'] + [pardict['tstop']+2*pardict['tmeas']]
        for ii in range(len(switchtimes)):
            if switchtimes[ii] > t:
                if type(vmin) == type([]):
                    vmin = vmin[ii]
                if type(vplus) == type([]):
                    vplus = vplus[ii]
                if type(vtm) == type([]):
                    vtm = vtm[ii]
                if type(rres) == type([]):
                    rres = rres[ii]
                if type(rcat) == type([]):
                    rcat = rcat[ii]
                if type(rcatGap) == type([]):
                    rcatGap = rcatGap[ii]
                break
    gapbandratio = (rcat*(vmin+vtm)-rres*(vplus-vtm))**2/(rcatGap*(vmin+vtm)-rres*(vplus-vtm))**2
    return gapbandratio

def estimate_density(ys,ls,H,binsize = 0.5):
    nbins = int(round(H/binsize))
    density = np.zeros(nbins)
    for ii in range(len(ys)):
        y = ys[ii]
        l = ls[ii]
        current_bin = int(y/binsize)
        density[current_bin] += l/binsize

    bin_ys = np.arange(0,H,binsize) + binsize/2
    return bin_ys, density

def find_max(freq,Pxx):
    highest = 0
    f = np.nan
    for ii in range(len(freq)):
        if Pxx[ii] > highest:
            highest = Pxx[ii]
            f = freq[ii]

    return f
    
def nGapsAbove(ys, ls, distance, minlength = 0):
    l_last = -1
    y_last = None

    iis = np.argsort(ys)
    N = 0
    for jj in range(len(iis)):
        ii = iis[jj]
        y = ys[ii]
        l = ls[ii]
        
        if l > minlength:
            if l_last > minlength:
                d = y - y_last
                if d > distance:
                    N += 1
            l_last = l
            y_last = y

    return N

def largestGapAverage(ys, ls, nGaps = 10, minlength = 0):
    l_last = -1
    y_last = None

    iis = np.argsort(ys)
    dists = []
    for jj in range(len(iis)):
        ii = iis[jj]
        y = ys[ii]
        l = ls[ii]
        
        if l > minlength:
            if l_last > minlength:
                d = y - y_last
                dists += [d]
            l_last = l
            y_last = y

    return np.mean(-np.partition(-np.array(dists),nGaps)[:nGaps])

    
def plotSummaryQuantities(filename, labels = None, outname = None, fontsize = 40, histogramOnly = False, normalise = True, burntime = -1, convertToHours = True, t0 = 0, labelQuantity = '', labelUnit = '', bandcutoff = 0.25, vplusIso = None, binsize = 0.5, GapDistance = 1, minlength = 0, nGaps = 5, plotbanddensity = True, leaveout_bandgaps = [], boxplotlabels = None, ylimEmpty = None, nbins = 60, plotMTdensityHistogram = False, window = 10, scaleBoxplots = False, plotLegends = True):
    if type(filename) == type(''):
        combifilenames = [[filename]]
    elif type(filename[0]) == type(''):
        combifilenames = [filename]
    else:
        combifilenames = filename
    if outname == None:
        outname = combifilenames[0][0]
    if labels == None:
        labels = ['' for ii in range(len(combifilenames))]
    if boxplotlabels == None:
        boxplotlabels = labels

    plotdata = []
    plotdata2 = []
    plotdata25 = []
    plotdata3 = []
    plotdata4 = []
    plotdata7 = []
    plotdata8 = []
    plotdata9 = []
    plotdata10 = []
    plotdata11 = []
    plotdata12 = []
    plotBoundFraction = True
    plotGDPGTPratio = True
    for kk in range(len(combifilenames)):# variable pars
        filenames = combifilenames[kk]
        filename = filenames[0]

        # get basic details
        pardict = read_pars(filename)
##        if not pardict['useBands']:
##            raise ValueError('System does not have bands')
        nBands = pardict['nBands']
        bandwidth = pardict['bandwidth']
        H = pardict['H']
        W = pardict['W']
        totalBandwidth = nBands*bandwidth
        totalGapwidth = H-totalBandwidth
        gapwidth = totalGapwidth/nBands
        period = bandwidth + gapwidth
        bandstartpos = gapwidth/2
        bandendpos = bandstartpos + bandwidth

        f = h5py.File(filename + '.hdf5','r')
        t_list = f['t'][:]
        f.close()
        
        combinedSDs = []
        combinedBandgapratios = []
        combinedEnhancements = []
        combinedBanddensities = []
        combinedNucleationFractions = []
        combinedGDPGTPratios = []
        combinedEmptyBands = []
        combinedNucleationRates = []
        combinedUniformities = []
        combinedFrequencies = []
        combinedDensitySDs = []

        for filename in filenames:# reps
            sds = []
            bandgapratios = []
            enhancements = []
            banddensities = []
            nucFracs = []
            gdpgtpratios = []
            emptybands = []
            nucleations = []
            uniformities = []
            frequencies = []
            densitySDs = []
            densitywindow = []
            for ii in range(0, len(t_list)):# timepoints
                frame = ii + 1
                f = h5py.File(filename + '.hdf5','r')
                Glengths = f['Glengths_m' + str(frame)][:]
                Slengths = f['Slengths_m' + str(frame)][:]
                lengths = np.append(Glengths,Slengths)
                Gy = f['Gy_m' + str(frame)][:]
                Sy = f['Sy_m' + str(frame)][:]
                ys = np.append(Gy,Sy)
                eventcounters = f['eventCounters_m' + str(frame)][:]
                if pardict['tubulinMode'] == 'Local2' and plotGDPGTPratio:
                    tub1 = f['tubulinconcentration1_m' + str(frame)][:]
                    tub2 = f['tubulinconcentration2_m' + str(frame)][:]
                    totTub1 = sum(tub1)
                    totTub2 = sum(tub2)
                    gdpgtpratios += [totTub2/totTub1]
                else:
                    plotGDPGTPratio = False
                f.close()

                pos = ys%period
                lengthsBands = lengths[np.logical_and(pos>bandstartpos, pos<bandendpos)]
                lengthsGaps = lengths[np.logical_or(pos<bandstartpos, pos>bandendpos)]

                # Calculate Band/Gap ratio
                if t_list[ii] >= burntime and sum(lengthsBands) == 0:
                    if not kk in leaveout_bandgaps:
                        leaveout_bandgaps += [kk]
                        print 'Warning: found replicate containing empty bands. Dropping variables from summary.'
                bandgapRatio = totalBandwidth/totalGapwidth * np.array(sum(lengthsGaps))/np.array(sum(lengthsBands))
                bandgapratios += [bandgapRatio]

                # Calculate separation enhancement
                expectedRatio = equilibriumGapBandRatioIso(pardict,t_list[ii],vplusIso = vplusIso)
                enhancement = expectedRatio/bandgapRatio
                if ii != 0 and (bandgapRatio == 0 or np.isnan(bandgapRatio) or np.isinf(bandgapRatio)):
                    print 'Warning, unable to calculate enchancements at t = %.0f for certain replicates'%t_list[ii]
                enhancements += [enhancement]

                # Calculate MT density bands
                if plotbanddensity:
                    banddensity = sum(lengthsBands)/totalBandwidth/W
                else:
                    banddensity = (sum(lengthsBands)+sum(lengthsGaps))/(H*W)
                banddensities += [banddensity]

                # Calculate uniformity
                if len(ys) > 0:
##                    uniformity = stats.kstest(ys, stats.uniform(loc=0.0, scale=H).cdf)[0]
                    uniformity = nGapsAbove(ys,lengths,GapDistance,minlength)
                else:
                    uniformity = np.nan
                uniformities += [uniformity]

                # estimate periodicity
##                bin_ys, density = estimate_density(ys, lengths, H, binsize = binsize)
##                freq,Pxx = periodogram(density, fs = 1/binsize)
##                frequencies += [find_max(freq,Pxx)]
                if len(ys) > nGaps+1:
                    frequency = largestGapAverage(ys, lengths, nGaps, minlength)
                else:
                    frequency = np.nan
                frequencies += [frequency]

                # SD of time averaged density histogram
                if plotMTdensityHistogram:
                    # find total length densities in 1 um intervals
                    ybins, totallengths, barheights = getLengthDensities(pardict, lengths, ys, nbins = nbins)
                else:
                    # find total lengths in bands and gaps
                    ybins, totallengths, barheights = getLengthDensities(pardict, lengths, ys)
                    
                if ii >= window:
                    average = np.array(densitywindow).mean(0)
                    if plotMTdensityHistogram:
                        densitySDs += [average.std()/average.mean()]
                    else:
                        bandvalues = average[range(1,len(average),2)]
                        densitySDs += [bandvalues.std()/bandvalues.mean()]
                    densitywindow[ii%window] = totallengths
                else:
                    densitywindow += [totallengths]

                # Calculate equal distribution
                lengths = lengthsBands
                ys = ys[np.logical_and(pos>bandstartpos, pos<bandendpos)]
                totalbandlength = sum(lengths)
                expectedFraction = 1/nBands
                bandfractions = []
                for band in range(nBands):
                    if totalbandlength == 0:
                        bandfractions += [expectedFraction]
                    else:
                        bandfractions += [sum(lengths[np.logical_and(ys>(band*period), ys<((band+1)*period))])/totalbandlength]
                squares = [(elem-expectedFraction)**2 for elem in bandfractions]
                ssq = sum(squares)
                if normalise:
                    ssq = np.sqrt(ssq)*np.sqrt(nBands/(nBands-1))
                sds += [ssq]

                # Calculate bound nucleation fraction
                if pardict['MTdependentNucleation'] in ('NucleationComplexes','Unrestricted') and plotBoundFraction:
                    if ii == 0:
                        nucFracs += [np.nan]
                    else:
                        dBound = eventcounters[4] - previousEventcounters[4]
                        dFree = eventcounters[2] - previousEventcounters[2]
                        nucFrac = dBound/(dBound+dFree)
                        nucFracs += [nucFrac]
                else:
                    plotBoundFraction = False

                # Number of empty bands
                counter = 0
                for band in range(nBands):
                    counter += sum(lengths[np.logical_and(ys>(band*period), ys<((band+1)*period))]) <= bandcutoff*totalbandlength/nBands
                emptybands += [counter]

                # Nucleation rates
                if ii == 0:
                    nucleations += [np.nan]
                else:
                    dBound = eventcounters[4] - previousEventcounters[4]
                    dFree = eventcounters[2] - previousEventcounters[2]
                    dNuc = dBound + dFree
                    dt = t_list[ii] -  t_list[ii-1]
                    rnAvg = dNuc/dt/(H*W)
                    nucleations += [rnAvg]
                previousEventcounters = eventcounters
                
                
            combinedSDs += [sds]
            combinedBandgapratios += [bandgapratios]
            combinedEnhancements += [enhancements]
            combinedBanddensities += [banddensities]
            combinedNucleationFractions += [nucFracs]
            combinedGDPGTPratios += [gdpgtpratios]
            combinedEmptyBands += [emptybands]
            combinedNucleationRates += [nucleations]
            combinedUniformities += [uniformities]
            combinedFrequencies += [frequencies]
            average = np.array(densitywindow).mean(0)
            if plotMTdensityHistogram:
                densitySDs += [average.std()/average.mean()]
            else:
                bandvalues = average[range(1,len(average),2)]
                densitySDs += [bandvalues.std()/bandvalues.mean()]
            combinedDensitySDs += [densitySDs]
            
        plotdata += [combinedSDs]
        plotdata2 += [combinedBandgapratios]
        plotdata25 += [combinedBanddensities]
        plotdata3 += [combinedNucleationFractions]
        plotdata4 += [combinedGDPGTPratios]
        plotdata7 += [combinedEmptyBands]
        plotdata8 += [combinedNucleationRates]
        plotdata9 += [combinedEnhancements]
        plotdata10 += [combinedUniformities]
        plotdata11 += [combinedFrequencies]
        plotdata12 += [combinedDensitySDs]

    fig1 = plt.figure()
    ax1 = fig1.gca()
    fig3 = plt.figure()
    ax3 = fig3.gca()
    fig25 = plt.figure()
    ax25 = fig25.gca()
    burntime -= t0
    if convertToHours:
        burntime /= 3600
    if len(combifilenames[0]) == 1:
        for ii in range(len(combifilenames)):
            ax1.plot(t_list,plotdata[ii][0])
            ax25.plot(t_list,plotdata25[ii][0])
            ax3.plot(t_list,plotdata2[ii][0])
            figs = [fig1,fig3]
    else:
        fig2 = plt.figure()
        ax2 = fig2.gca()
        fig4 = plt.figure()
        ax4 = fig4.gca()
        fig26 = plt.figure()
        ax26 = fig26.gca()
        fig7 = plt.figure()
        ax7 = fig7.gca()
        fig75 = plt.figure()
        ax75 = fig75.gca()
        fig8 = plt.figure()
        ax8 = fig8.gca()
        fig85 = plt.figure()
        ax85 = fig85.gca()
        fig9 = plt.figure()
        ax9 = fig9.gca()
        fig95 = plt.figure()
        ax95 = fig95.gca()
        fig10 = plt.figure()
        ax10 = fig10.gca()
        fig105 = plt.figure()
        ax105 = fig105.gca()
        fig11 = plt.figure()
        ax11 = fig11.gca()
        fig115 = plt.figure()
        ax115 = fig115.gca()
        fig12 = plt.figure()
        ax12 = fig12.gca()
        fig125 = plt.figure()
        ax125 = fig125.gca()
        fig13 = plt.figure()
        ax13 = fig13.gca()
        fig135 = plt.figure()
        ax135 = fig135.gca()
        boxplotdata = []
        boxplotdata2 = []
        boxplotdata25 = []
        boxplotdata7 = []
        boxplotdata8 = []
        boxplotdata9 = []
        boxplotdata10 = []
        boxplotdata11 = []
        boxplotdata12 = []
        if (pardict['MTdependentNucleation'] in ('NucleationComplexes','Unrestricted') and plotBoundFraction) or (pardict['tubulinMode'] == 'Local2' and plotGDPGTPratio):
            fig5 = plt.figure()
            ax5 = fig5.gca()
            fig6 = plt.figure()
            ax6 = fig6.gca()
            boxplotdata3 = []
        for ii in range(len(combifilenames)):
            filename = combifilenames[ii][0]
            f = h5py.File(filename + '.hdf5','r')
            t_list = f['t'][:]
            f.close()
            t_list = np.array(t_list)-t0
            if convertToHours:
                t_list /= 3600
            combinedSDs = np.array(plotdata[ii])
            averageSDs = combinedSDs.mean(0)
            sdSDs =  combinedSDs.std(0)
            ax1.plot(t_list,averageSDs, label = labels[ii])
            ax1.fill_between(t_list,averageSDs-sdSDs,averageSDs+sdSDs,alpha=0.3)

            averageSDs = combinedSDs.mean(1)
            boxplotdata += [averageSDs]

            # bandgap plots
            combinedBandgapratios = np.array(plotdata2[ii])
            averageBandgapratios = combinedBandgapratios.mean(0)
            sdBandgapratios = combinedBandgapratios.std(0)
            ax3.plot(t_list,averageBandgapratios, label = labels[ii])
            ax3.fill_between(t_list,averageBandgapratios-sdBandgapratios,averageBandgapratios+sdBandgapratios,alpha=0.3)

            for jj in range(len(t_list)):
                if t_list[jj] >= burntime:
                    break
            if not ii in leaveout_bandgaps:
                combinedBandgapratios = combinedBandgapratios[:,jj:]
                averageBandgapratios = combinedBandgapratios.mean(1)
                boxplotdata2 += [averageBandgapratios]

            # band density plots
            combinedBanddensities = np.array(plotdata25[ii])
            averageBanddensities = combinedBanddensities.mean(0)
            sdBanddensities = combinedBanddensities.std(0)
            ax25.plot(t_list,averageBanddensities, label = labels[ii])
            ax25.fill_between(t_list,averageBanddensities-sdBanddensities,averageBanddensities+sdBanddensities,alpha=0.3)

            combinedBanddensities = combinedBanddensities[:,jj:]
            averageBanddensities = combinedBanddensities.mean(1)
            boxplotdata25 += [averageBanddensities]

            # empty band plots
            combinedEmptyBands = np.array(plotdata7[ii])
            averageEmptyBands = combinedEmptyBands.mean(0)
            sdEmptyBands = combinedEmptyBands.std(0)
            ax7.plot(t_list,averageEmptyBands, label = labels[ii])
            ax7.fill_between(t_list,averageEmptyBands-sdEmptyBands,averageEmptyBands+sdEmptyBands,alpha=0.3)
            semEmptyBands = sdEmptyBands/np.sqrt(combinedEmptyBands.shape[0])
            ax13.plot(t_list,averageEmptyBands, label = labels[ii])
            ax13.fill_between(t_list,averageEmptyBands-semEmptyBands,averageEmptyBands+semEmptyBands,alpha=0.3)

            combinedEmptyBands = combinedEmptyBands[:,jj:]
            averageEmptyBands = combinedEmptyBands.mean(1)
            boxplotdata7 += [averageEmptyBands]

            # nucleation rate plots
            combinedRn = np.array(plotdata8[ii])
            averageRn = combinedRn.mean(0)
            sdRn = combinedRn.std(0)
            ax8.plot(t_list,averageRn, label = labels[ii])
            ax8.fill_between(t_list,averageRn-sdRn,averageRn+sdRn,alpha=0.3)

            combinedRn = combinedRn[:,jj:]
            averageRn = combinedRn.mean(1)
            boxplotdata8 += [averageRn]

            # separation enhancement plots
            combinedEnh = np.array(plotdata9[ii])
            averageEnh = combinedEnh.mean(0)
            sdEnh = combinedEnh.std(0)
            ax9.plot(t_list,averageEnh, label = labels[ii])
            ax9.fill_between(t_list,averageEnh-sdEnh,averageEnh+sdEnh,alpha=0.3)

            combinedEnh = combinedEnh[:,jj:]
            averageEnh = combinedEnh.mean(1)
            boxplotdata9 += [averageEnh]

            # uniformity plots
            combinedUni = np.array(plotdata10[ii])
            averageUni = combinedUni.mean(0)
            sdUni = combinedUni.std(0)
            ax10.plot(t_list,averageUni, label = labels[ii])
            ax10.fill_between(t_list,averageUni-sdUni,averageUni+sdUni,alpha=0.3)

            combinedUni = combinedUni[:,jj:]
            averageUni = combinedUni.mean(1)
            boxplotdata10 += [averageUni]

            # frequency plots
            combinedF = np.array(plotdata11[ii])
            averageF = combinedF.mean(0)
            sdF = combinedF.std(0)
            ax11.plot(t_list,averageF, label = labels[ii])
            ax11.fill_between(t_list,averageF-sdF,averageF+sdF,alpha=0.3)

            combinedF = combinedF[:,jj:]
            averageF = combinedF.mean(1)
            boxplotdata11 += [averageF]

            # density histogram sd plots
            combinedDsd = np.array(plotdata12[ii])
            averageDsd = combinedDsd.mean(0)
            sdDsd = combinedDsd.std(0)
            ax12.plot(t_list[(window-1):],averageDsd, label = labels[ii])
            ax12.fill_between(t_list[(window-1):],averageDsd-sdDsd,averageDsd+sdDsd,alpha=0.3)

            combinedDsd = combinedDsd[:,jj:]
            averageDsd = combinedDsd.mean(1)
            boxplotdata12 += [averageDsd]
            
            # bound nucleation plots
            if pardict['MTdependentNucleation'] in ('NucleationComplexes','Unrestricted') and plotBoundFraction:
                combinedNucleationFractions = np.array(plotdata3[ii])
                averageNucleationFractions = combinedNucleationFractions.mean(0)
                sdNucleationFractions = combinedNucleationFractions.std(0)
                ax5.plot(t_list,averageNucleationFractions, label = labels[ii])
                ax5.fill_between(t_list,averageNucleationFractions-sdNucleationFractions,averageNucleationFractions+sdNucleationFractions,alpha=0.3)

                combinedNucleationFractions = combinedNucleationFractions[:,jj:]
                averageNucleationFractions = combinedNucleationFractions.mean(1)
                boxplotdata3 += [averageNucleationFractions]
            # GDP/GTP-ratio plots
            elif pardict['tubulinMode'] == 'Local2' and plotGDPGTPratio:
                combinedRatios = np.array(plotdata4[ii])
                averageRatios = combinedRatios.mean(0)
                sdRatios = combinedRatios.std(0)
                ax5.plot(t_list,averageRatios,label = labels[ii])
                ax5.fill_between(t_list,averageRatios-sdRatios,averageRatios+sdRatios, alpha = 0.3)

                combinedRatios = combinedRatios[:,jj:]
                averageRatios = combinedRatios.mean(1)
                boxplotdata3 += [averageRatios]

        if scaleBoxplots:
            scalefactor = 0.2+0.16*len(labels)
        else:
            scalefactor = 1
        ax2.boxplot(boxplotdata, labels = boxplotlabels, widths = 0.5)
        ax26.boxplot(boxplotdata25, labels = boxplotlabels, widths = 0.5)
        boxplotlabels2 = np.delete(boxplotlabels,leaveout_bandgaps)
        ax4.boxplot(boxplotdata2, labels = boxplotlabels2, widths = 0.5)
        ax75.boxplot(boxplotdata7, labels = boxplotlabels, widths = 0.5)
        ax85.boxplot(boxplotdata8, labels = boxplotlabels, widths = 0.5)
        ax95.boxplot(boxplotdata9, labels = boxplotlabels, widths = 0.5)
        ax105.boxplot(boxplotdata10, labels = boxplotlabels, widths = 0.5)
        ax115.boxplot(boxplotdata11, labels = boxplotlabels, widths = 0.5)
        ax125.boxplot(boxplotdata12, labels = boxplotlabels, widths = 0.5)
        ax135.boxplot(boxplotdata7, labels = boxplotlabels, widths = 0.5)
        if (pardict['MTdependentNucleation'] in ('NucleationComplexes','Unrestricted') and plotBoundFraction) or (pardict['tubulinMode'] == 'Local2' and plotGDPGTPratio):
            ax6.boxplot(boxplotdata3, labels = boxplotlabels, widths = 0.5)

        if (pardict['MTdependentNucleation'] in ('NucleationComplexes','Unrestricted') and plotBoundFraction) or (pardict['tubulinMode'] == 'Local2' and plotGDPGTPratio):
            figs = fig1,fig2,fig25,fig26,fig3,fig4,fig5,fig6,fig7,fig75,fig8,fig85,fig9,fig95,fig10,fig105,fig11,fig115,fig12,fig125,fig13,fig135
        else:
            figs = fig1,fig2,fig25,fig26,fig3,fig4,fig7,fig75,fig8,fig85,fig9,fig95,fig10,fig105,fig11,fig115,fig12,fig125,fig13,fig135

    ax3.axhline(1, linestyle = '--', color = 'k')
    ax9.axhline(1, linestyle = '--', color = 'k')
    if t0 > 0:
        ax3.axvline(0, linestyle = '--', color = 'k')
        ax1.axvline(0, linestyle = '--', color = 'k')
        ax25.axvline(0, linestyle = '--', color = 'k')
        ax7.axvline(0, linestyle = '--', color = 'k')
        ax8.axvline(0, linestyle = '--', color = 'k')
        ax9.axvline(0, linestyle = '--', color = 'k')
        ax10.axvline(0, linestyle = '--', color = 'k')
        ax11.axvline(0, linestyle = '--', color = 'k')
        ax12.axvline(0, linestyle = '--', color = 'k')
        if pardict['MTdependentNucleation'] in ('NucleationComplexes','Unrestricted') and plotBoundFraction:
            ax5.axvline(0, linestyle = '--', color = 'k')

    if labelQuantity:
        title = labelQuantity
        if labelUnit:
             title += ' (' + labelUnit + ')'
    else:
        title = None

    for ii in range(1,len(figs),2):
        ax = figs[ii].gca()
        for label in ax.get_xticklabels():
            label.set_rotation(45)
            label.set_fontsize(fontsize/2)
        if title:
            ax.set_xlabel(title, fontsize = fontsize/2)

    if title:
        title = title + ':'

    for ii in range(0,len(figs),2):
        ax = figs[ii].gca()
        ax.set_xlim(left=t_list[0])
        if convertToHours:
            ax.set_xlabel('$t$ (h)', fontsize = fontsize)
        else:
            ax.set_xlabel('$t$ (s)', fontsize = fontsize)
        ax.tick_params(axis='x', which='major', labelsize=fontsize*0.7)
        ax.tick_params(axis='y', which='major', labelsize=fontsize*0.7)
        if plotLegends:
            legend = ax.legend(fontsize=fontsize/2, loc = 'upper right', title = title)
            plt.setp(legend.get_title(),fontsize=fontsize/2)
        if not (ax is ax9):
            ax.set_ylim(bottom=0)
    if ax3.get_ylim()[1] > 1.2:
        ax3.set_ylim(top=1.2)
    if ylimEmpty != None:
        ax7.set_ylim(top = ylimEmpty)
        ax13.set_ylim(top = ylimEmpty)
    ax12.set_xlim(left=t_list[0]+(t_list[1]-t_list[0])*window)
    ax9.set_yscale('log')
    ax1.set_ylabel('Deviation from equal bands', fontsize = fontsize)
    ax2.set_ylabel('Deviation from equal bands', fontsize = fontsize/2)
    ax3.set_ylabel('Gap:Band density', fontsize = fontsize)
    ax4.set_ylabel('Gap:Band density', fontsize = fontsize/2)
    if plotbanddensity:
        ax25.set_ylabel('Average MT density bands', fontsize = fontsize)
        ax26.set_ylabel('Average MT density bands', fontsize = fontsize/2)
    else:
        ax25.set_ylabel('Average MT density', fontsize = fontsize)
        ax26.set_ylabel('Average MT density', fontsize = fontsize/2)
    ax7.set_ylabel('Number of "empty" bands', fontsize = fontsize)
    ax75.set_ylabel('Number of "empty" bands', fontsize = fontsize/2)
    ax8.set_ylabel('Average nucleation rate', fontsize = fontsize)
    ax85.set_ylabel('Average nucleation rate', fontsize = fontsize/2)
    ax9.set_ylabel('Separation enhancement', fontsize = fontsize)
    ax95.set_ylabel('Separation enhancement', fontsize = fontsize/2)
##    ax10.set_ylabel('Uniformity', fontsize = fontsize)
##    ax105.set_ylabel('Uniformity', fontsize = fontsize/2)
    ax10.set_ylabel('Gaps > $%s$ $\mu m$'%str(GapDistance), fontsize = fontsize)
    ax105.set_ylabel('Gaps > $%s$ $\mu m$'%str(GapDistance), fontsize = fontsize/2)
##    ax11.set_ylabel('Frequency', fontsize = fontsize)
##    ax115.set_ylabel('Frequency', fontsize = fontsize/2)
    ax11.set_ylabel('Mean size largest %d gaps'%nGaps, fontsize = fontsize)
    ax115.set_ylabel('Mean size largest %d gaps'%nGaps, fontsize = fontsize/2)
    ax12.set_ylabel('Persistent heterogeneity', fontsize = fontsize)
    ax125.set_ylabel('Persistent heterogeneity', fontsize = fontsize/2)
    ax13.set_ylabel('Number of "empty" bands', fontsize = fontsize)
    ax135.set_ylabel('Number of "empty" bands', fontsize = fontsize/2)
    if pardict['MTdependentNucleation'] in ('NucleationComplexes','Unrestricted') and plotBoundFraction:
        ax5.set_ylabel('Bound nucleation fraction', fontsize = fontsize)
        ax6.set_ylabel('Bound nucleation fraction', fontsize = fontsize/2)
    elif pardict['tubulinMode'] == 'Local2' and plotGDPGTPratio:
        ax5.set_ylabel('GDP/GTP-ratio', fontsize = fontsize)
        ax6.set_ylabel('GDP/GTP-ratio', fontsize = fontsize/2)

    figlabels = ['_Distrubution','_boxplotsDist','_density','_boxplotDensity','_BandGap','_boxplotsBandGap']
    if pardict['MTdependentNucleation'] in ('NucleationComplexes','Unrestricted') and plotBoundFraction:
        figlabels += ['_BoundFraction','_boxplotsBoundFraction']
    elif pardict['tubulinMode'] == 'Local2' and plotGDPGTPratio:
        figlabels += ['_gdpgtpRatio','_boxplotsgdpgtpRatio']
    figlabels += ['_emptyBands','_boxplotEmpty','_rn','_boxplotrn','_enhancement','_boxplotEnhancement','_Ngaps','_boxplotNgaps','_gapsize','_boxplotGapsize','_sdDensity','_boxplotSDdensity','_emptyBandsSEM','_boxplotsEmptyBandsSEM']
    for ii in range(0,len(figs),2):
        fig = figs[ii]
        default_size = fig.get_size_inches()
        fig.set_size_inches( (default_size[0]*2, default_size[1]*2) )
    for ii in range(1,len(figs),2):
        fig = figs[ii]
        default_size = fig.get_size_inches()
        fig.set_size_inches( (scalefactor*default_size[0], default_size[1]) )
        
    for ii in range(len(figs)):
        fig = figs[ii]
        fig.tight_layout()
        fig.savefig(outname + figlabels[ii] + '.png')
        plt.close(fig)


