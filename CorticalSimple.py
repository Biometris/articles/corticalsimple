#!/usr/bin/python
from __future__ import division
import numpy as np
from numpy.random import RandomState
import h5py
import time
import sys
from heapq import heappush, heappop
from bandsolver import bandsolver
import warnings


def nearlyEqual(a,b, epsilonmultiplier = 10):
    eps = epsilonmultiplier*sys.float_info.epsilon
    diff = abs(a-b)
    if a == b:
        return True
    elif abs(a) < eps or abs(b) < eps:
        return diff < eps
    else:
        return diff/min(abs(a),abs(b)) < eps

def saveRandomState(filename,rst):
    f = h5py.File(filename,'a')
    if 'rst1' in f:
        del f['rst1']
    f.create_dataset('rst1', data = [rst[0]])
    if 'rst2' in f:
        del f['rst2']
    f.create_dataset('rst2', data = [rst[1]])
    if 'rst3' in f:
        del f['rst3']
    f.create_dataset('rst3', data = [rst[2]])
    if 'rst4' in f:
        del f['rst4']
    f.create_dataset('rst4', data = [rst[3]])
    if 'rst5' in f:
        del f['rst5']
    f.create_dataset('rst5', data = [rst[4]])
    f.close()

def retrieveRandomState(filename):
    f = h5py.File(filename,'r')
    try:
        rst1 = f['rst1'][:][0]
        rst2 = f['rst2'][:][0]
        rst3 = f['rst3'][:][0]
        rst4 = f['rst4'][:][0]
        rst5 = f['rst5'][:][0]
        rst = (rst1,rst2,rst3,rst4,rst5)
    except KeyError:
        rst = None
    f.close()
    return rst

def get_cyy(c,dy):
    # find neighbours
    cimin1 = np.roll(c,1)
    ciplus1 = np.roll(c,-1)

    # handle zero flux BCs
    cimin1[0] = c[0]
    ciplus1[-1] = c[-1]

    # determine second spatial derivative
    cyy = (cimin1+ciplus1-2*c)/dy**2

    return cyy


def getRegionalMTs(regions):
    MTs = []
    for region in regions:
        MTs += [mt for mt in region.mts]
    return MTs

def getNeighbouringLength(MTs, ypos,dx,t,up = True):
    result = 0
    for mt in MTs:
        if up:
            diff = mt.ypos - ypos
        else:
            diff =  ypos - mt.ypos
        if diff > 0 and diff < dx:
            mt.updateLength(t)
            result += mt.length
    return result

def getRhoMT(mts,xpos,ypos,t,pardict):
    # local MT density calculation
    # currently average across x-axis
    dx = 0.5*pardict['densityResolution']
    dy = pardict['dy']
    H = pardict['H']
    Nregions = int(round(H/dy))
    
    # correction in case ypos==H (occurs when upper boundary is selected)
    if ypos == pardict['H']:
        ypos -= 10*sys.float_info.epsilon
        
    # determine regionindex
    regionindex = int(ypos/dy)

    # obtain regions of appropriate index
    regions = [mtset.regions[regionindex] for mtset in mts]

    # obtain all MTs in regions
    MTs = getRegionalMTs(regions)

    # get neighbouring length in region
    totallength = getNeighbouringLength(MTs, ypos, dx, t, up = True)

    # get distance to boundary of region
    examinedDistance = (regionindex+1)*dy - ypos

    newRegionIndex = regionindex
    while examinedDistance < dx and newRegionIndex < (Nregions-1):
        # move to next region
        newRegionIndex += 1
        regions = [mtset.regions[newRegionIndex] for mtset in mts]

        # obtain all MTs in regions
        MTs = getRegionalMTs(regions)

        # get neighbouring length in region
        totallength += getNeighbouringLength(MTs, ypos, dx, t, up = True)

        # get distance to boundary of region
        examinedDistance += dy

    # repeat in downward direction
    # obtain regions of appropriate index
    regions = [mtset.regions[regionindex] for mtset in mts]

    # obtain all MTs in regions
    MTs = getRegionalMTs(regions)

    # get neighbouring length in region
    totallength += getNeighbouringLength(MTs, ypos, dx, t, up = False)

    # get distance to boundary of region
    examinedDistance = ypos - regionindex*dy

    newRegionIndex = regionindex
    while examinedDistance < dx and newRegionIndex > 0:
        # move to next region
        newRegionIndex -= 1
        regions = [mtset.regions[newRegionIndex] for mtset in mts]

        # obtain all MTs in regions
        MTs = getRegionalMTs(regions)

        # get neighbouring length in region
        totallength += getNeighbouringLength(MTs, ypos, dx, t, up = False)

        # get distance to boundary of region
        examinedDistance += dy


    # height of area analysed
    h = 2*dx
    # compensate for boundaries
    if ypos + dx > H:
        h -= (ypos+dx-H)
    if ypos - dx < 0:
        h -= (dx-ypos)
        
    # return local MT density
    return totallength/(h*pardict['W'])


# Determine if any MT from the current region is within a certain radius
def regionalMTinArea(MTs, xpos, ypos, r, t, W):
    result = 0
    for mt in MTs:
        diff = abs(mt.ypos - ypos)
        if diff < r:
            mt.updateXpos(t)

            # calculate x-component of radius
            dx = np.sqrt(r**2-diff**2)
            
            # calculate left en right boundaries of the 'bubble'
            xleft = xpos - dx
            xright = xpos + dx

            # test for all possible situations where the MT is in the 'bubble'
            if xleft < mt.xpos1 and mt.xpos1 < xright:
                # left MT-end in bubble
                return True
            if mt.xpos1 < xleft and mt.xpos2 > xleft:
                # left MT-end left of bubble, right MT-end past bubble start
                return True
            if mt.xpos2 > W and (mt.xpos2 - W) > xleft:
                # right MT-end loops around and passes left side of bubble
                return True
            if xleft < 0 and mt.xpos2 > (xleft + W):
                # right MT-end inside part of bubble sticking out on the left
                return True
            if xright > W and mt.xpos1 < (xright - W):
                # left MT-end inside part of bubble sticking out on the right
                return True

    return False

# Determine if there is any MT within a certain radius
def MTinArea(mts,xpos,ypos,t,pardict):
    # local MT density calculation
    # currently average across x-axis
    r = pardict['searchradius']
    dy = pardict['dy']
    H = pardict['H']
    W = pardict['W']
    Nregions = int(round(H/dy))
    
    # correction in case ypos==H (occurs when upper boundary is selected)
    if ypos == H:
        ypos -= 10*sys.float_info.epsilon
        
    # determine regionindex for lowest position that could be in area
    regionindex = int((ypos-r)/dy)
    if regionindex < 0:
        regionindex = 0

    # obtain regions of appropriate index
    regions = [mtset.regions[regionindex] for mtset in mts]
    
    # obtain all MTs in regions
    MTs = getRegionalMTs(regions)
    
    # determine if any MTs in the region fall within the radius
    if regionalMTinArea(MTs, xpos, ypos, r, t, W):
        return True
    
    # get distance to boundary of region
    examinedDistance = (regionindex+1)*dy - (ypos-r)

    newRegionIndex = regionindex
    while examinedDistance < (2*r) and newRegionIndex < (Nregions-1):
        # move to next region
        newRegionIndex += 1
        regions = [mtset.regions[newRegionIndex] for mtset in mts]

        # obtain all MTs in regions
        MTs = getRegionalMTs(regions)

        # determine if any MTs in the region fall within the radius
        if regionalMTinArea(MTs, xpos, ypos, r, t, W):
            return True

        # update examined distance
        examinedDistance += dy
   
    return False


def testMTinArea():
    W = 7.5*2*np.pi
    H = 60
    dy = 0.1
    Nregions = int(round(H/dy))
    pardict = {}
    pardict['trackRegionalDensity'] = True
    pardict['useSpatialRegions'] = True
    pardict['useRegionalTubulin'] = False
    pardict['useNucleationComplexes'] = True
    pardict['useMTjumpingNCs'] = True
    pardict['vplus'] = 1
    pardict['vmin'] = 1
    pardict['vtm'] = 1
    pardict['dy'] = dy
    pardict['tubulinMode'] = 'None'
    pardict['rng'] = np.random.RandomState()
    r = 0.5
    pardict['searchradius'] = r
    pardict['H'] = H
    pardict['W'] = W
    
    
    mts = [MTset(True, pardict,Nregions=Nregions),MTset(False, pardict,Nregions=Nregions)]
    N = 150
    L = 10
    Nnc = 100

    for ii in range(N):
        setindex = int(round(np.random.rand()))
        ypos = np.random.uniform(0,H)
        xpos1 = np.random.uniform(0,W)
        l = np.random.exponential(10)
        xpos2 = xpos1 + l

        newMT = Microtubule(l, ypos, 0, 1, mts[setindex], xpos = xpos1)
        mts[setindex].addMT(newMT,0)


    mtsG, mtsS = mts
    Glengths = np.array([mt.length for mt in mtsG.mts])
    Slengths = np.array([mt.length for mt in mtsS.mts])
    Gy = np.array([mt.ypos for mt in mtsG.mts])
    Sy = np.array([mt.ypos for mt in mtsS.mts])
    Gxpos1s = np.array([mt.xpos1 for mt in mtsG.mts])
    Sxpos1s = np.array([mt.xpos1 for mt in mtsS.mts])
    

    import matplotlib.pyplot as plt
    fig = plt.figure()
    ax = fig.gca()
    
    ax.hlines(Gy,Gxpos1s,Gxpos1s+Glengths,colors = 'b', label = 'Growing MTs', zorder = 1)
    ax.hlines(Sy,Sxpos1s,Sxpos1s+Slengths,colors = 'r', label = 'Shrinking MTs', zorder = 2)
    ax.hlines(Gy,0,Gxpos1s+Glengths-W,colors = 'b', zorder = 3)
    ax.hlines(Sy,0,Sxpos1s+Slengths-W,colors = 'r', zorder = 4)

    ax.set_ylim(bottom=0,top=H)
    ax.set_xlim(left = 0, right = W)
    ax.set_aspect('equal', adjustable='box')

    default_size = fig.get_size_inches()
    fig.set_size_inches( (default_size[0]*2*0.75, default_size[1]*3*0.75) )
    fig.tight_layout(pad = 4, w_pad=0)

    for ii in range(Nnc):
        xpos = np.random.uniform(0,W)
        ypos = np.random.uniform(0,H)

        hit = MTinArea(mts,xpos,ypos,0,pardict)
        if hit:
            c = 'g'
        else:
            c = 'r'

        circle = plt.Circle((xpos, ypos), r, color=c, clip_on=False)
        ax.add_artist(circle)

        if hit:
            c = 'c'
        else:
            c = 'm'
        if xpos - r < 0:
            circle = plt.Circle((xpos+W, ypos), r, color=c, clip_on=False)
            ax.add_artist(circle)
        if xpos + r > W:
            circle = plt.Circle((xpos-W, ypos), r, color=c, clip_on=False)
            ax.add_artist(circle)

    
    plt.show()
    


class NucleationComplex(object):
    def __init__(self,ypos,owner,index,regions,regionindex,MTbound = False,boundMT = None,boundindex=None, xpos = None):
        self.ypos = ypos
        self.MTbound = MTbound
        self.boundMT = boundMT
        self.boundindex = boundindex # index in list of bound NCs of associated MT
        self.owner = owner
        self.index = index # index in list of nucleation complexes
        self.regionindex = regionindex # index of region in list of regions
        self.regions = regions # MT containing regions corresponding to the position of this MT

        if self.owner.pardict['useMTjumpingNCs']:
            rng = self.owner.pardict['rng']
            W = self.owner.pardict['W']
            if xpos == None:
                self.xpos = rng.uniform(0,W)
            else:
                self.xpos = xpos
            self.boundxpos = None # relative x-position on bound MT

    def diffuse(self,rng,d,W,H,t,up=None,right=None, oldMT = None):
        # draw new step size if none is given
        if up == None:
            up = d*rng.normal()
        if self.owner.dim2 and right == None:
            right = d*rng.normal()

        # move NC by stepsize in y-direction
        oldypos = self.ypos
        self.ypos += up

        # test if boundaries were exceeded and compensate appropriately
        if self.ypos > H:
            boundaryHit = True
            reflectedstep = H-self.ypos
            self.ypos = H
        elif self.ypos < 0:
            boundaryHit = True
            reflectedstep = -self.ypos
            self.ypos = 0
        else:
            boundaryHit = False

        # Determine if a MT was hit
        # get MTs in region
        MTs = self.getMTs()

        # determine first collision with MT
        hitMT = self.determineHit(MTs,rng,up,W,t, right = right, oldypos = oldypos, oldMT = oldMT)

        # determine regionindex in case of no collisions
        newRegionIndex = int(self.ypos/self.owner.pardict['dy'])
        # correction in case ypos==H (occurs when upper boundary is hit)
        if newRegionIndex == self.owner.Nregions:
            newRegionIndex -= 1

        # while there are no collisions and full step has not been completed:
        while hitMT == None and abs(self.regionindex-newRegionIndex)>0:
            # step one region further
            if up > 0:
                self.regionindex += 1
            else:
                self.regionindex -= 1
            self.regions = [mtset.regions[self.regionindex] for mtset in self.owner.mts]
            
            # get MT positions of new region
            MTs = self.getMTs()

            # determine first collision in new region
            hitMT = self.determineHit(MTs,rng,up,W,t, right = right, oldypos = oldypos, oldMT = oldMT)

        if hitMT != None:
            self.MTbound = True
            self.boundMT = hitMT
            self.ypos = hitMT.ypos
            self.boundindex = len(hitMT.boundNCs)
            hitMT.boundNCs.append(self)
            # update x-position
            self.xpos += (self.ypos - oldypos)/up*right
            # compensate for periodic boundaries
            if self.xpos < 0:
                self.xpos += W
            elif self.xpos > W:
                self.xpos -= W
            if self.owner.pardict['useMTjumpingNCs']:
                self.boundxpos = self.xpos - hitMT.xpos1
                if self.boundxpos < 0:
                    # compensate for wrapping MTs
                    self.boundxpos += W
                    # determine existence of overlap
                    remaininglength = hitMT.xpos2 - self.xpos - W
                    overlap = int(remaininglength/W)
                else:
                    # determine existence of overlap
                    remaininglength = hitMT.xpos2 - self.xpos
                    overlap = int(remaininglength/W)
                # if there is overlap: randomly select which part is bound
                if overlap > 0:
                    overlap = rng.randint(0,overlap+1)
                    self.boundxpos += W*overlap
                
        elif boundaryHit:
            if self.owner.dim2:
                # determine how much to step right
                completedright = (self.ypos-oldypos)/up*right
                # update x-position
                self.xpos += completedright
                # compensate for periodic boundaries
                if self.xpos < 0:
                    self.xpos += W
                elif self.xpos > W:
                    self.xpos -= W
                # update right to remaining step
                right -= completedright
            else:
                right = None
            # update up to remaining step
            up = reflectedstep
            self.diffuse(rng,d,W,H,t,up=up,right=right)

        elif self.owner.dim2:
            # update x-position
            self.xpos += right
            # compensate for periodic boundaries
            if self.xpos < 0:
                self.xpos += W
            elif self.xpos > W:
                self.xpos -= W
            
    def determineHit(self,MTs,rng,up,W,t,right=None,oldypos=None,oldMT=None):
        # returns the position of the first MT that was hit, None otherwise
        dmax = -1
        useMTjumpingNCs = self.owner.pardict['useMTjumpingNCs']
        rskip = self.owner.pardict['rskip']
        hitMT = None
        for mt in MTs:
            if oldMT != None and (mt is oldMT):
                # ignore MT that you're hopping off
                continue
            if up > 0:
                diff = (self.ypos-mt.ypos)
            else:
                diff = (mt.ypos-self.ypos)
            if diff < abs(up) and diff >= 0: # test for hit
                # test for closest hit (largest diff!)
                # diff now guaranteed positive and smaller than step size)
                if diff > dmax:
                    if rskip == 0 or rng.rand() > rskip: # skip MTs at a certain rate
                        if useMTjumpingNCs:
                            mt.updateXpos(t)
                            if self.owner.dim2:
                                # calculate x-position where hit would occur
                                xposhit = self.xpos + (mt.ypos - oldypos)/up*right
                                # compensate hit position for periodic BCs
                                if xposhit < 0:
                                    xposhit += W
                                elif xposhit > W:
                                    xposhit -= W
                            else:
                                xposhit = self.xpos
                            # skip MTs that are absent at NCs x-position
                            if xposhit > mt.xpos1 and xposhit < mt.xpos2:
                                dmax = diff
                                hitMT = mt
                            elif mt.xpos2 > W and xposhit < (mt.xpos2-W):
                                dmax = diff
                                hitMT = mt
                        else:
                            dmax = diff
                            hitMT = mt
        return hitMT

    def getMTs(self):
        MTs = []
        for region in self.regions:
            MTs += [mt for mt in region.mts]
        return MTs

    def checkMTbinding(self,t):
        # update x-positions of bound MT and release nc if necessary
        self.boundMT.updateXpos(t)

    def unbindMT(self):
        self.MTbound = False
        self.boundMT = None
        self.boundindex = None
        if self.owner.pardict['useMTjumpingNCs']:
            self.boundxpos = None

    def suicide(self):
        if self.boundMT != None:
            self.boundMT.unbindNC(self)
        self.owner.deleteSingleItem(self)

    def detachMT(self, t):
        if self.boundMT == None:
            raise ValueError('Attempting to detach NC without bound MT')

        # Remove NC from list of bound NCs of corresponding MT
        oldMT = self.boundMT
        self.boundMT.unbindNC(self)

        # Remove MT-bound status
        self.unbindMT()

        # Displace ypos to avoid instant rebind
        rng = self.owner.pardict['rng']
        W = self.owner.pardict['W']
        H = self.owner.pardict['H']
        d = self.owner.steplength
        move_up = rng.randint(0,2)

        if move_up:
            # Step of 1e-6 of sd of normal step
            up = d/1e6
        else:
            up = -d/1e6

        # use diffuse function to prevent accidental MT skipping or moving out of bounds
        self.diffuse(rng,d,W,H,t,up=up,right=0,oldMT = oldMT)
        


class NucleationComplexes(object):
    def __init__(self,mts,pardict,Nregions = None):
        self.ncs = []
        self.mts = mts
        self.pardict = pardict
        if Nregions == None:
            self.Nregions = mts[0].Nregions
        else:
            self.Nregions = Nregions
        if pardict['useMTjumpingNCs']:
            self.dim2 = True
        else:
            self.dim2 = False
        self.steplength = np.sqrt(2*pardict['dt']*pardict['Dnc'])

    def __len__(self):
        return len(self.ncs)

    def getNbound(self):
        nbound = 0
        for nc in self.ncs:
            if nc.MTbound:
                nbound += 1
        return nbound

    def bindToMembrane(self,ypos = None, xpos = None):
        if ypos == None:
            # generate random position
            ypos = self.pardict['rng'].uniform(0,self.pardict['H'])

        # determine regionindex corresponding to position
        regionindex = int(ypos/self.pardict['dy'])

        # find regions corresponding with index
        regions = [mtset.regions[regionindex] for mtset in self.mts]

        # create new MT and add to list
        newNC = NucleationComplex(ypos,self,len(self),regions,regionindex, xpos = xpos)
        self.ncs.append(newNC)

    def detachRandomNC(self, t, rng):
        # find total number of bound nucleation complexes
        NboundNCs = self.getNbound()
        
        # select random complex for unbinding
        item = rng.randint(0,NboundNCs)
        nc = self.getBoundNC_at(item, NboundNCs)

        # detach NC from its bound MT
        nc.detachMT(t)

    def getBoundNC_at(self, ii, length):
        # Guess to start at the front or at the back
        if ii < length/2:
            jj = 0
            for nc in self.ncs:
                if nc.MTbound:
                    if ii == jj:
                        return nc
                    else:
                        jj += 1
        else:
            jj = length - 1
            for n in range(len(self.ncs)-1,-1,-1):
                nc = self.ncs[n]
                if nc.MTbound:
                    if ii == jj:
                        return nc
                    else:
                        jj -= 1

    def deleteSingleItem(self,nc):
        index = nc.index
        N = len(self)
        if index != N-1:
            # move last item to position of item to be deleted
            self.ncs[index] = self.ncs[-1]
            # update index of item that was moved
            self.ncs[index].index = index
        # delete item at last position
        del self.ncs[-1]

    def diffusion(self,t):
        rng = self.pardict['rng']
        W = self.pardict['W']
        H = self.pardict['H']

        # when using x-positions, determine if bound ncs are still bound
        for nc in self.ncs:
            if nc.MTbound:
                nc.checkMTbinding(t)

        # move all non-MTbound NCs
        for nc in self.ncs:
            if not nc.MTbound:
                nc.diffuse(rng,self.steplength,W,H,t)
    
def testCaseDiffusion():
    D = 2
    dt = 0.01
    H = 60
    W = 40
    Tend = 10
    yposStart = 30
    xposStart = 20
    regions = []
    index = 0
    dy = 0.5
    regionindex = int(yposStart/dy)
    Nregions = int(round(H/dy))
    rng = RandomState()
    useMTjumpingNCs = True

    pardict = {}
    pardict['Dnc'] = D
    pardict['dt'] = dt
    pardict['H'] = H
    pardict['W'] = W
    pardict['rng'] = rng
    pardict['dy'] = dy
    pardict['useMTjumpingNCs'] = useMTjumpingNCs
    dim = 2

    mts = []
    ncs = NucleationComplexes(mts,pardict,Nregions = Nregions)
    ncs.ncs.append(NucleationComplex(yposStart,ncs,index,regions,regionindex))
    ncs.ncs[0].xpos = xposStart
    
    maxsteplength = (2*dt*D)**0.5
    N = int(round(Tend/dt))
    t = 0
    rsquareds = []
    rsquareds_theory = []
    for ii in range(N):
        t += dt
        ncs.diffusion(t)
        rsquareds += [(ncs.ncs[0].ypos-yposStart)**2+(ncs.ncs[0].xpos-xposStart)**2]
        rsquareds_theory += [2*dim*t*D]

    return rsquareds,rsquareds_theory

def testDiffusion():
    Nreps = 1000
    sumofsquares = 0
    for ii in range(Nreps):
        rsquareds,rsquareds_theory = testCaseDiffusion()
        sumofsquares += np.array(rsquareds)

    r = (sumofsquares/Nreps)/rsquareds_theory
    import matplotlib.pyplot as plt
    fig= plt.figure()
    ax=fig.gca()
    ax.plot(range(len(r)),r)
    plt.show()

    


class TubulinConcentrations(object):
    def __init__(self,pardict,Nregions,tubulinConc1,tubulinConc2 = 0):
        self.pardict = pardict
        self.Nregions = Nregions
        self.cT1 = np.zeros(Nregions) + tubulinConc1
        if self.pardict['tubulinMode'] == 'Local2':
            # cT1 = GTP-tubulin, cT2 = GDP-tubulin
            self.cT2 = np.zeros(Nregions) + tubulinConc2
        self.mode = self.pardict['integrationMethod']

        self.lastLmax = pardict['Lmax']

    def linkMTs(self,mts):
        self.mts = mts

    def getReactionTerm(self,t):
        A = self.pardict['H']*self.pardict['W']
        fMTdt = np.zeros(self.Nregions)
        
        # determine difference in MT of each region and convert to tubulin concentration
        for ii in range(self.Nregions):
            dl = 0
            for mt in self.mts:
                dl += mt.regions[ii].getLengthDifference(t)
            fMTdt[ii] = - dl*self.Nregions/A

        return fMTdt

    def getReactionTerm2(self,t):
        A = self.pardict['H']*self.pardict['W']
        fMTdt1 = np.zeros(self.Nregions)
        fMTdt2 = np.zeros(self.Nregions)

        # determine difference in MT of each region and convert to tubulin concentration
        for ii in range(self.Nregions):
            dl1 = 0
            dl2 = 0
            for mtset in self.mts:
                growth,shrinkage = mtset.regions[ii].updateGrowthcounter(t)
                dl1 += growth
                dl2 += shrinkage
            fMTdt1[ii] = - dl1*self.Nregions/A
            fMTdt2[ii] = dl2*self.Nregions/A

        return fMTdt1,fMTdt2

    def updateConcentrations(self,t):
        dt = self.pardict['dt']
        dy = self.pardict['dy']
        Dtub = self.pardict['Dtub']

        if self.pardict['tubulinMode'] == 'Local1':
            # determine reactionterm
            fMTdt = self.getReactionTerm(t)
            
            # perform (reaction-)diffusion step
            if self.mode == 'FTCS':
                self.stepFTCS(dt,dy,Dtub,fMTdt)
            elif self.mode == 'CrankNicolson':
                self.stepCrankNicolson(dt,dy,Dtub,fMTdt)
            else:
                raise NotImplementedError('Integration mode %s is not implemented'%self.mode)
            
        elif self.pardict['tubulinMode'] == 'Local2':
            Dtub2 = self.pardict['Dtub2']
            beta = self.pardict['beta']
            
            # determine MT-part of reaction term
            fMTdt1,fMTdt2 = self.getReactionTerm2(t)
            
            # perform reaction-diffusion step
            if self.mode == 'FTCS':
                self.stepFTCS2(dt,dy,Dtub,Dtub2,beta,fMTdt1,fMTdt2)
            elif self.mode == 'CrankNicolson':
                self.stepCrankNicolson2(dt,dy,Dtub,Dtub2,beta,fMTdt1,fMTdt2)
            else:
                raise NotImplementedError('Integration mode %s is not implemented'%self.mode)
            
        else:
            raise NotImplementedError('Tubulin mode %s is not yet implemented' % self.pardict['tubulinMode'])

    def stepFTCS(self,dt,dy,Dtub,fMTdt):
        # get second order spatial derivative of tubulin concentration
        cyy = get_cyy(self.cT1,dy)
        
        # calculate new concentrations
        self.cT1 += dt*Dtub*cyy + fMTdt

    def stepCrankNicolson(self,dt,dy,Dtub,fMTdt):
        npointsy = self.Nregions

        # get second order spatial derivative of tubulin concentration
        cyy = get_cyy(self.cT1,dy)

        # calculate righthandside of the matrix-vector product
        Gst = self.cT1 + 0.5*Dtub*dt*cyy + fMTdt
        
        # Obtain single band matrix and solve matrix-vector product
        Qst = self.Qy(npointsy,Dtub,dt,dy)
        self.cT1 = bandsolver(Qst,Gst, periodic=False, check_finite = False, overwrite_b = True)

    def stepFTCS2(self,dt,dy,Dtub,Dtub2,beta,fMTdt1,fMTdt2):
        # get second order spatial derivative of tubulin concentration
        cyy = get_cyy(self.cT1,dy)
        cyy2 = get_cyy(self.cT2,dy)
        
        # calculate new concentrations
        new_cT1 = self.cT1 + dt*Dtub*cyy + dt*beta*self.cT2 + fMTdt1
        new_cT2 = self.cT2 + dt*Dtub2*cyy2 - dt*beta*self.cT2 + fMTdt2

        # update state variables
        self.cT1 = new_cT1
        self.cT2 = new_cT2

    def stepCrankNicolson2(self,dt,dy,Dtub,Dtub2,beta,fMTdt1,fMTdt2):
        npointsy = self.Nregions

        # get second order spatial derivative of tubulin concentration
        cyy = get_cyy(self.cT1,dy)
        cyy2 = get_cyy(self.cT2,dy)

        # calculate righthandside of the matrix-vector product
        Gst = self.cT1 + 0.5*Dtub*dt*cyy + dt*beta*self.cT2 + fMTdt1
        Gst2 = self.cT2 + 0.5*Dtub2*dt*cyy2 - dt*beta*self.cT2 + fMTdt2
        
        # Obtain single band matrix and solve matrix-vector product
        Qst = self.Qy(npointsy,Dtub,dt,dy)
        Qst2 = self.Qy(npointsy,Dtub2,dt,dy)
        self.cT1 = bandsolver(Qst,Gst, periodic=False, check_finite = False, overwrite_b = True)
        self.cT2 = bandsolver(Qst2,Gst2, periodic=False, check_finite = False, overwrite_b = True)

        
    # Obtain band matrices (Q) for implicit part of Crank-Nicolson algorithm
    def Qy(self, npointsy, Dyyst, dt, dy):
        # homogeneous diffusion cases: r=scalar and a single Q matrix is needed
        r = 0.5*Dyyst*dt/dy**2
        offdiagmin1 = -r
        offdiag1 = offdiagmin1
        diag = 1+2*r

        # Fill band matrix for bandsolver function
        Q = np.zeros((3,npointsy))
        Q[1,:] = diag
        Q[0,1:] = offdiagmin1
        Q[2,:-1] = offdiag1
        
        # Handle boundary conditions (zero-flux)
        Q[1,0] -= r
        Q[1,-1] -= r
            
        return Q

    def testCase(self,filename,dt,left=True):
        H = 20
        dy = 0.1
        npointsy = int(round(H/dy))
        self.Nregions = npointsy
        y = np.arange(0,H,dy)
        self.cT1 = np.zeros(npointsy)+1
        D = 1
        Tend = 500
        N = int(round(Tend/dt))

        if self.mode == 'FTCS':
            fD = self.stepFTCS
        else:
            fD = self.stepCrankNicolson

        mm = 0
        t=0
        tlist = []
        while t < Tend:
            if (t%25)<1e-5:
                mm+=1
                tlist += [t]
                if t == 0:
                    mode = 'w'
                else:
                    mode = 'a'
                f = h5py.File(filename + '.hdf5',mode)
                f.create_dataset('u_m' + str(mm),data = self.cT1)
                f.close()
            if left:
                self.cT1[0]=0
            else:
                self.cT1[-1]=0
            fD(dt,dy,D,np.zeros(npointsy))
            t+=dt

        mm+=1
        tlist += [t]
        f = h5py.File(filename + '.hdf5','a')
        f.create_dataset('u_m' + str(mm),data = self.cT1)
        f.create_dataset('tlist',data = tlist)
        f.create_dataset('ydata',data = y)
        f.close()

        

class DeterministicQueue(object):
    def __init__(self):
        self.queue = []

    def push(self, item):
        heappush(self.queue, item)

    def pop(self):
        return heappop(self.queue)

    def first(self):
        return self.queue[0]

    def __len__(self):
        return len(self.queue)

    def resetDeathTimes(self, vmin, vtm, t):
        newqueue = []
        for event in self.queue:
            if event.valid:
                event.resetDeathTime(vmin, vtm, t)
                heappush(newqueue, event)
        self.queue = newqueue

class DeterministicEvent(object):
    def __init__(self,time,et,mt=None,valid=True):
        self.eventTime = time
        self.eventType = et
        self.mt = mt
        self.valid = valid

    def __str__(self):
        return str(self.time)

    def __lt__(self,other):
        return self.eventTime < other.eventTime

    def invalidate(self):
        self.valid = False

    def resetDeathTime(self, vmin, vtm, t):
        if self.eventType == 'MTdeath':
            vshrink = vmin
            if self.mt.owner.treadmilling:
                vshrink += vtm
            else:
                raise ValueError('katanin not supported for changing vmin')
            self.eventTime = self.mt.lastupdate + self.mt.length/vshrink
            if self.eventTime < t:
                raise ValueError('Reset death time is in the past.\n Event time = %f\nSystem time = %f\nResidual MT length = %f\nLast MT length update = %f\nvmin = %f\nvmin = %f'%(self.eventTime,t,self.mt.length,self.mt.lastupdate,self.mt.owner.pardict['vvmin'], vmin))

    def execute(self,t):
        if self.eventType == 'MTdeath':
            # unbind nucleation complexes
            if self.mt.owner.pardict['useNucleationComplexes']:
                for nc in self.mt.boundNCs:
                    nc.unbindMT()
            # kill microtubule
            self.mt.length = 0
            self.mt.suicide(t)
        else:
            raise ValueError('Unknown event type %s'%str(self.eventType))

class Region(object):
    def __init__(self, netSpeed, owner, pardict, index, t = 0, N = 0, length = 0, tubulinconcentrations = None):
        self.v = netSpeed
        self.owner = owner
        self.pardict = pardict
        self.lastupdate = t
        self.lasttimestep = t
        self.number = N
        self.length = length
        self.lastlength = length # for tracking length differences (single tubulin case)
        self.growthcounter = 0 # for tracking total growth per time step (two tubulin case)
        self.shrinkagecounter = 0 # for tracking total shrinkage per time step (two tubulin case)
        self.index = index # index in list of regions
        self.tubulinconcentrations = tubulinconcentrations
        self.mts = []

    def adjustGrowthcounter(self,t,positive):
        if self.owner.growing:
            vg = self.v
            vs = 0
        else:
            vg = 0
            vs = -self.v
        if self.owner.treadmilling and self.owner.growing:
            vg += self.pardict['vtm']
            vs += self.pardict['vtm']

        dt = t-self.lasttimestep
        if positive:
            self.growthcounter += vg*dt
            self.shrinkagecounter += vs*dt
        else:
            self.growthcounter -= vg*dt
            self.shrinkagecounter -= vs*dt

    def updateGrowthcounter(self,t):
        dt = t-self.lasttimestep
        if self.owner.growing:
            vg = self.v
            vs = 0
        else:
            vg = 0
            vs = -self.v
        if self.owner.treadmilling and self.owner.growing:
            vg += self.pardict['vtm']
            vs += self.pardict['vtm']
        self.growthcounter += self.number*vg*dt
        self.shrinkagecounter += self.number*vs*dt

        result = self.growthcounter,self.shrinkagecounter
        
        self.growthcounter = 0
        self.shrinkagecounter = 0
        self.lasttimestep = t

        return result

    def updateLength(self,t):
        dt = t - self.lastupdate
        self.length += dt*self.v*self.number
        self.lastupdate = t

    def updateNetSpeed(self, constant, vtm, local = False):
        if local:
            self.v = constant*self.tubulinconcentrations.cT1[self.index] - vtm
        else:
            vplus = constant
            self.v = vplus - vtm
        if self.v < 0 and self.owner.growing:
            raise ValueError('Growing MTs are net shrinking!\nvplus = %.04f\tcT1 = %.03f'%(self.v+vtm,self.tubulinconcentrations.cT1[self.index]))

    def getLengthDifference(self,t):
        self.updateLength(t)
        dl = self.length - self.lastlength
        self.lastlength = self.length
        return dl

    def addMT(self,mt,t):
        # for the two tubulin case adjust the growth counter
        if self.pardict['tubulinMode'] == 'Local2':
            self.adjustGrowthcounter(t,positive=False)
            
        # first update region length
        self.updateLength(t)

        # then add MT length to region
        self.length += mt.length

        # add MT to region
        self.mts.append(mt)

        # update MT region
        mt.region = self
        mt.regionindex = self.number
        self.number += 1

    def removeMT(self,index,t):
        # for the two tubulin case adjust the growth counter
        if self.pardict['tubulinMode'] == 'Local2':
            self.adjustGrowthcounter(t,positive=True)
        
        # first update region length
        self.updateLength(t)

        # then remove MT length from region
        self.length -= self.mts[index].length

        # then remove MT from region
        N = self.number
        if index != N-1:
            # move last item to position of item to be deleted
            self.mts[index] = self.mts[-1]
            # update index of item that was moved
            self.mts[index].regionindex = index
        # delete item at last position
        del self.mts[-1]
        
        self.number -= 1

    def findDensityPos(self, densityPos, t):
        if self.pardict['useNucleationSaturation']:
            # draw a new random number for the position within this region
            densityPos = self.pardict['rng'].uniform(0,self.length)
        # search regions for the one that holds the cutoff value
        # guess whether to start from front or back
        if densityPos < self.length/2:
            totalDensity = 0
            for ii in range(self.number):
                # update total length of MT
                self.mts[ii].updateLength(t)
                # check if cutoff position falls on MT
                totalDensity += self.mts[ii].length
                if totalDensity > densityPos:
                    return self.mts[ii].ypos
        else:
            totalDensity = self.length
            for ii in range(self.number-1,-1,-1):
                # update total length of MT
                self.mts[ii].updateLength(t)
                # check if cutoff position falls on MT
                totalDensity -= self.mts[ii].length
                if totalDensity < densityPos:
                    return self.mts[ii].ypos
                
        # This should not happen
        raise ValueError('No microtubule found at densityposition %.1f.\nTotal density = %.1f.'%(densityPos,self.length))
        

class Microtubule(object):
    def __init__(self, length, ypos, lastupdate, index, owner, regionindex = 0, region = None, xpos = None):
        self.length = length
        self.ypos = ypos
        self.lastupdate = lastupdate
        self.owner = owner # the MTset object containing this MT
        self.region = region # the region to which the MT belongs
        self.index = index # the position of this MT in the MTset object that contains it
        self.regionindex = regionindex # position of this MT in its region object
        self.deathEvent = None # new MTs start out growing

        if owner.pardict['useNucleationComplexes']:
            self.boundNCs = []
            if owner.pardict['useMTjumpingNCs']:
                self.lastXposUpdate = lastupdate
                # 50/50 forward and backward growing MTs
                if owner.pardict['rng'].randint(0,2):
                    self.forward = True
                else:
                    self.forward = False
                if xpos != None:
                    self.xpos1 = xpos
                    self.xpos2 = xpos
                    if length > 0:
                        self.xpos2 = xpos+length
                else:
                    raise ValueError('No x-position passed for MT construction')

    def updateLength(self,t):
        # calculate time since last update
        dt = t-self.lastupdate
        # calculate total change in length since last update
        dl = 0
        if self.owner.pardict['useRegionalTubulin']:
            netSpeed = self.region.v
            dl = dt*netSpeed
        else:
            if self.owner.growing:
                vplus = self.owner.vplus
                dl += dt*vplus
            else:
                vmin = self.owner.pardict['vmin']
                dl -= dt*vmin
            if self.owner.treadmilling:
                vtm = self.owner.pardict['vtm']
                dl -= dt*vtm

        # update MT length and time of last update
        self.length += dl
        self.lastupdate = t

        # update x-positions if these are used
        if self.owner.pardict['useMTjumpingNCs']:
            self.updateXpos(t)

        if self.owner.pardict['useGlobalTubulin'] and self.owner.growing:
            if self.length < 0:
                raise ValueError('Growing MT deletion!')
                
    def updateXpos(self,t):
        # calculate time since last update
        dt = t-self.lastXposUpdate
        # calculate position speeds
        if self.owner.pardict['useRegionalTubulin']:
            netSpeed = self.region.v
            if self.owner.treadmilling:
                vtm = pardict['vtm']
                v1 = vtm
                v2 = netSpeed + vtm
            else:
                v1 = 0
                v2 = netSpeed
        else:
            if self.owner.growing:
                v2 = self.owner.vplus
            else:
                v2 = -self.owner.pardict['vmin']
            if self.owner.treadmilling:
                v1 = self.owner.pardict['vtm']

        # update x-positions
        if self.forward:
            self.xpos1 += v1*dt
            self.xpos2 += v2*dt
            for nc in self.boundNCs:
                nc.boundxpos -= v1*dt
        else:
            self.xpos1 -= v2*dt
            self.xpos2 -= v1*dt
            for nc in self.boundNCs:
                nc.boundxpos += v2*dt

        # guarantee xpos1 is within boundaries
        W = self.owner.pardict['W']
        while self.xpos1 > W:
            self.xpos1 -= W
            self.xpos2 -= W
        if not self.forward: # this one never happens for retracting minus ends
            while self.xpos1 < 0:
                self.xpos1 += W
                self.xpos2 += W

        # test if bound NCs are still bound and release them if not
        # walk backwards to prevent breaking loop upon deletions!
        for ii in range(len(self.boundNCs)-1,-1,-1):
            nc = self.boundNCs[ii]
            if nc.boundxpos < 0:
                self.unbindNC(nc)
                nc.unbindMT()
            elif nc.boundxpos > (self.xpos2-self.xpos1):
                self.unbindNC(nc)
                nc.unbindMT()

        # update time of last update
        self.lastXposUpdate = t
    
    def setDeathEvent(self,t):
        if not self.owner.growing:
            # calculate total shrinking speed
            v = self.owner.pardict['vmin']
            if self.owner.treadmilling:
                v += self.owner.pardict['vtm']
            # calculate death event time
            tdeath = self.lastupdate + self.length/v
            if tdeath < t:
                raise ValueError('Calculated time of death is in the past.\ntdeath = '+str(tdeath)+'\nsystem time = '+str(t))
            # create new death event
            self.deathEvent = DeterministicEvent(tdeath, 'MTdeath', self)
        else:
            self.deathEvent = None
        return self.deathEvent

    def clearDeathEvent(self):
        # invalidate death event, then release it
        if self.deathEvent != None:
            self.deathEvent.invalidate()
            self.deathEvent = None

    def changeOwner(self,newOwner,newIndex):
        self.owner = newOwner
        self.index = newIndex

    def suicide(self,t):
        # delete this object from the set and region that hold it
        self.owner.deleteSingleItem(self,t)

    def unbindNC(self,nc):
        index = nc.boundindex
        N = len(self.boundNCs)
        if index != N-1:
            # move last item to position of item to be deleted
            self.boundNCs[index] = self.boundNCs[-1]
            # update index of item that was moved
            self.boundNCs[index].boundindex = index
        # delete item at last position
        del self.boundNCs[-1]


class MTset(object):
    def __init__ (self, growing, pardict, treadmilling = True, lengths = None, ys = None,t = None, Nregions = 100, tubulinconcentrations = None):
        self.growing = growing
        self.pardict = pardict
        self.treadmilling = treadmilling
        self.mts = []
        if lengths:# In theory. Probably best not initialise non-empty objects.
            for ii in range(len(lengths)):
                mt = Microtubule(lengths[ii], ys[ii], t[ii], ii, self)
                self.mts.append(mt)

        self.number = len(self.mts)
        if self.pardict['trackRegionalDensity']:
            self.density = sum([mt.length for mt in self.mts])
            self.lastupdate = 0 # last update of density (total length)

            if self.growing:
                v = pardict['vplus']
            else:
                v = -pardict['vmin']
            if self.treadmilling:
                v -= pardict['vtm']
            self.regions = [Region(v, self, self.pardict, ii, tubulinconcentrations = tubulinconcentrations) for ii in range(Nregions)]
            if not pardict['useSpatialRegions']:
                self.lastregion = -1
            self.Nregions = Nregions

        if not pardict['useRegionalTubulin']:
            self.vplus = pardict['vplus']

    def __len__(self):
        return len(self.mts)

    def getTotalStoredLength(self):
        # only sensible if system is up to date!
        density = 0
        for mt in self.mts:
            density += mt.length
        return density

    def updateTotalLength(self,t):
        if self.pardict['useRegionalTubulin']:
            for region in self.regions:
                region.updateLength(t)
            self.density = sum([region.length for region in self.regions])
        else:
            dt = t - self.lastupdate
            v = 0
            if self.growing:
                v += self.vplus
            else:
                v -= self.pardict['vmin']
            if self.treadmilling:
                v -= self.pardict['vtm']
            self.density += dt*v*self.number
        self.lastupdate = t

    def getDensityForNuc(self,t):
        if self.pardict['useNucleationSaturation']:
            density = 0
            for region in self.regions:
                if region.length < self.pardict['saturationLength']:
                    if not self.pardict['useRegionalTubulin']:
                        region.updateLength(t) # already done for regional tubulin
                    density += region.length
                else:
                    density += self.pardict['saturationLength']
        else:
            density = self.density
        return density

    def updateGlobalVplus(self,vplus):
        self.vplus = vplus
        if self.treadmilling and vplus < self.pardict['vtm']:
            raise ValueError('Growing MTs are net shrinking')

    def updateRegionalVplus(self,constant):
        if self.treadmilling:
            vtm = self.pardict['vtm']
        else:
            vtm = 0
        if self.pardict['useGlobalTubulin']:
            for region in self.regions:
                region.updateNetSpeed(constant,vtm)
        elif self.pardict['useRegionalTubulin']:
            for region in self.regions:
                region.updateNetSpeed(constant,vtm,local = True)
        else:
            raise ValueError('local differences in tubulin not yet implemented')

    def updateRegionalNetspeed(self):
        if self.treadmilling:
            vtm = self.pardict['vtm']
        else:
            vtm = 0
        if self.growing:
            constant = self.pardict['vplus']
        else:
            constant = -self.pardict['vmin']
        for region in self.regions:
            region.updateNetSpeed(constant,vtm)

    def deleteSingleItem(self,mt,t):
        if self.pardict['trackRegionalDensity']:
            # remove MT from its region
            mt.region.removeMT(mt.regionindex,t)

            # first update total length
            self.updateTotalLength(t)
            
            # then remove MT length from total length
            self.density -= mt.length

        index = mt.index
        N = self.number
        if index != N-1:
            # move last item to position of item to be deleted
            self.mts[index] = self.mts[-1]
            # update index of item that was moved
            self.mts[index].index = index
        # delete item at last position
        del self.mts[-1]
        
        self.number -= 1

    def updateLengths(self,t):
        if self.pardict['trackRegionalDensity']:
            # update length in mtset
            self.updateTotalLength(t)
            if not self.pardict['useRegionalTubulin']:
                # update length in regions (part of updateTotalLength if regional tubulin is used)
                for region in self.regions:
                    region.updateLength(t)
        # update length of individual MTs
        N = self.number
        for ii in range(N):
            self.updateSingleLength(ii,t)

    def updateSingleLength(self,ii,t):
        # update length and get length difference
        self.mts[ii].updateLength(t)

    def selectRandomItem(self):
        rng = self.pardict['rng']
        N = self.number
        ii = rng.randint(0,N)
        return ii
  
    def switch(self,other,ii,t):
        # always update length before switching!
        self.updateSingleLength(ii,t)

        # delete mt from this set
        mt = self.mts[ii]
        self.deleteSingleItem(mt,t)

        # add mt to other set and collect new death event
        newDeathEvent = other.addMT(mt,t)
        
        # return the new death event
        return newDeathEvent

    def nucleate(self, t, ypos = None, xpos = None):
        # if no position is provided, draw one from a uniform distribution
        if ypos == None:
            H = self.pardict['H']
            rng = self.pardict['rng']
            ypos = rng.uniform(0,H)
            
        # create new microtubule
        newMT = Microtubule(0, ypos, t, self.number, self, xpos = xpos)

        # add the MT and return the new death event (should be None)
        return self.addMT(newMT,t)

    def addMT(self,mt,t):
        if self.pardict['trackRegionalDensity']:
            # find region to add MT to
            if self.pardict['useSpatialRegions']:
                index = int(mt.ypos/self.pardict['dy'])
                if index == self.Nregions: # compensate in case ypos==H
                    index -= 1
                region = self.regions[index]
            else:
                self.lastregion = (self.lastregion+1)%self.Nregions
                region = self.regions[self.lastregion]
            
            # add new MT to its region
            region.addMT(mt,t)
            
            # update the length of the set before adding new MT
            self.updateTotalLength(t)
            self.density += mt.length
            self.mts.append(mt)
        else:
            # just add new MT to set
            self.mts.append(mt)

        # update the owner of the MT
        mt.changeOwner(self, self.number)
        self.number += 1

        # invalidate existing death event
        mt.clearDeathEvent()

        # set new death event and return it
        return mt.setDeathEvent(t)

    def isInGap(self,item):
        period = self.pardict['period']
        bandstartpos = self.pardict['bandstartpos']
        bandendpos = self.pardict['bandendpos']
        y = self.mts[item].ypos
        pos = y%period
        return (pos < bandstartpos) or (pos > bandendpos)
        
    def findDensityPos(self,densityPos,t, totallength = None):
        if totallength == None:
            totallength = self.density
        # search regions for the one that holds the cutoff value
        # guess whether to start from front or back
        if densityPos < totallength/2:
            totalDensity = 0
            for ii in range(self.Nregions):
                # update regions length
                self.regions[ii].updateLength(t)
                # check if cumulative region length exceeds cutoff value
                regionlength = self.regions[ii].length
                if self.pardict['useNucleationSaturation']:
                    if regionlength > self.pardict['saturationLength']:
                        regionlength = self.pardict['saturationLength']
                totalDensity += regionlength
                if totalDensity > densityPos:
                    return self.regions[ii].findDensityPos(densityPos-totalDensity+regionlength,t)
        else:
            totalDensity = totallength
            for ii in range(self.Nregions-1,-1,-1):
                # update regions length
                self.regions[ii].updateLength(t)
                # check if cumulative region length exceeds cutoff value
                regionlength = self.regions[ii].length
                if self.pardict['useNucleationSaturation']:
                    if regionlength > self.pardict['saturationLength']:
                        regionlength = self.pardict['saturationLength']
                totalDensity -= regionlength
                if totalDensity < densityPos:
                    return self.regions[ii].findDensityPos(densityPos-totalDensity,t)
                
        # This should not happen
        raise ValueError('No region found at densityposition %.1f.\nTotal density = %.1f.'%(densityPos,self.density))

    

def getTimeInterval(mts,nucleationcomplexes,pardict,rng):#pars, nGN, nSN, nGT, nST,rand):
    # unpack variables
    useKataninRelease = pardict['useKataninRelease']
    useBands = pardict['useBands']
    useNucleationComplexes = pardict['useNucleationComplexes']
    useUnrestrictedNucleation = pardict['useUnrestrictedNucleation']
    useNCattracingMTs = pardict['useNCattractingMTs']
    if useKataninRelease:
        rrel = pardict['rrel']
        mtsGn,mtsGtm,mtsSn,mtsStm = mts
        NgrowingN = mtsGn.number
        NgrowingTM = mtsGtm.number
        NshrinkingN = mtsSn.number
        NshrinkingTM = mtsStm.number
        Ngrowing = NgrowingN + NgrowingTM
        Nshrinking = NshrinkingN + NshrinkingTM
        Nnew = NgrowingN+NshrinkingN
    else:
        mtsG,mtsS = mts
        Ngrowing = mtsG.number
        Nshrinking = mtsS.number
        rrel = 0
        Nnew = 0
    ru = pardict['ru']
    if ru > 0:
        NboundNCs = nucleationcomplexes.getNbound()
    else:
        NboundNCs = 0

    rres = pardict['rres']
    if useNucleationComplexes:
        Ncomplexes = len(nucleationcomplexes)
        if pardict['rnbound'] > pardict['rn']:
            # book nucleations at max rate
            rn = pardict['rnbound']*Ncomplexes
        else:
            rn = pardict['rn']*Ncomplexes
        if useNCattracingMTs:
            # book NC insertions at max rate
            rb = pardict['rbmax']*pardict['H']*pardict['W']
        else:
            rb = pardict['rb']*pardict['H']*pardict['W']
        rd = pardict['rd']
    else:
        rn = pardict['rnglobal']
        rb = 0
        rd = 0
        Ncomplexes = 0
    if useUnrestrictedNucleation:
        rn = pardict['rn_cap']
    rcat = pardict['rcat']
    if useBands:
        rcat = pardict['rcatGaps'] # book catastrophes at max rate

    # determine total stochastic event rate
    rate = rn + rres*Nshrinking + rcat*Ngrowing
    if useKataninRelease:
        rate += rrel*Nnew
    if useNucleationComplexes:
        rate += rb + rd*Ncomplexes
    if ru > 0:
        rate += ru*NboundNCs
    invRate= 1./rate
    
    # draw next stochastic event time
    nextEventInterval = rng.exponential(invRate)

    # determine the type of the next stochastic event
    eType = rng.rand()
    if eType < rn * invRate:
        return nextEventInterval,"nuc" 
    elif eType < (rn + rres*Nshrinking ) * invRate:
        return nextEventInterval,"res"
    elif useKataninRelease and eType < (rn + rres*Nshrinking + rrel*Nnew)*invRate:
        return nextEventInterval,"rel"
    elif useNucleationComplexes and eType < (rn + rres*Nshrinking + rrel*Nnew + rb)*invRate:
        return nextEventInterval,"bind"
    elif useNucleationComplexes and eType < (rn + rres*Nshrinking + rrel*Nnew + rb + rd*Ncomplexes)*invRate:
        return nextEventInterval,"unbind"
    elif ru > 0 and eType < (rn + rres*Nshrinking + rrel*Nnew + rb + rd*Ncomplexes + ru*NboundNCs)*invRate:
        return nextEventInterval,"hop"
    else:
        return nextEventInterval,"cat"

def redistributeNucleationPosition(pardict,ypos):
    period = pardict['period']
    nBands = pardict['nBands']
    H = pardict['H']
    rng = pardict['rng']
    return (ypos + rng.randint(nBands)*period)%H
    
def handleEvent(eventType, mts, detQ, nucleationcomplexes, t, pardict,eventCounters):
    useBands = pardict['useBands']
    useBoundNucleation = pardict['useBoundNucleation']
    useUnrestrictedNucleation = pardict['useUnrestrictedNucleation']
    useKataninRelease = pardict['useKataninRelease']
    useRedistributedNucleation = pardict['useRedistributedNucleation']
    useNucleationComplexes = pardict['useNucleationComplexes']
    tubulinMode = pardict['tubulinMode']
    useNCattractingMTs = pardict['useNCattractingMTs']
    useMTdependentInsertion = pardict['useMTdependentInsertion']
    rng = pardict['rng']
    if eventType == 'cat':
        # Find total number of growing MTs
        if useKataninRelease:
            NgrowingN = mts[0].number
            NgrowingTM = mts[1].number
            Ng = NgrowingN + NgrowingTM
        else:
            Ng = mts[0].number
        # select random growing MT for catastrophe
        item = rng.randint(0,Ng)
        if useKataninRelease:
            if item < NgrowingN:
                mt = mts[0]
                mt2 = mts[2]
            else:
                mt = mts[1]
                mt2 = mts[3]
                item -= NgrowingN
        else:
            mt = mts[0]
            mt2 = mts[1]
        # determine if catastrophe should still be executed
        executeCatastrophe = True
        if useBands:
            rcatFraction = pardict['rcatFraction']
            # skip catastrophes for bands to compensate for overbooking
            executeCatastrophe = False
            if mt.isInGap(item) or rng.rand() < rcatFraction:
                executeCatastrophe = True
        ##########################################
        # execute catastrophe if it is still valid
        if executeCatastrophe:
            # switch MT from growing to shrinking
            newDeathEvent = mt.switch(mt2,item,t)
            # book new death event
            if newDeathEvent != None:
                detQ.push(newDeathEvent)
            else:
                raise ValueError('No new death event booked after catastrophe')
            eventCounters[0] += 1
    elif eventType == 'res':
        # find total number of growing MTs
        if useKataninRelease:
            NshrinkingN = mts[2].number
            NshrinkingTM = mts[3].number
            Ns = NshrinkingN + NshrinkingTM
        else:
            Ns = mts[1].number
        # select random shrinking MT for rescue
        item = rng.randint(0,Ns)
        if useKataninRelease:
            if item < NshrinkingN:
                mt = mts[2]
                mt2 = mts[0]
            else:
                mt = mts[3]
                mt2 = mts[1]
                item -= NshrinkingN
        else:
            mt = mts[1]
            mt2 = mts[0]
        ################
        # execute rescue
        # switch MT from shrinking to growing
        newDeathEvent = mt.switch(mt2,item,t)
        # in future, book new death event (if growing mts can net shrink)
        if newDeathEvent != None:
            raise ValueError('Obtained death event upon rescue')
        eventCounters[1] += 1
    elif eventType == 'rel':
        if not pardict['useKataninRelease']:
            raise ValueError('Got a release event without using a katanin system')
        # select item and mtset regardles of existence
        NgrowingN = mts[0].number
        NshrinkingN = mts[2].number
        Nnew = NgrowingN + NshrinkingN
        item = rng.randint(0,Nnew)
        if item < NgrowingN:
            mt = mts[0]
            mt2 = mts[1]
        else:
            mt = mts[2]
            mt2 = mts[3]
            item -= NgrowingN
        # execute release
        newDeathEvent = mt.switch(mt2,item,t)
        # Book new death events for shrinking microtubules
        if newDeathEvent != None:
            detQ.push(newDeathEvent)
        eventCounters[3] += 1
    elif eventType == 'nuc':
        ypos = None
        xpos = None
        boundnucleation = False
        executeNucleation = True
        if useBoundNucleation and not useNucleationComplexes:
            # update Total lengths and determine total density
            rhos = []
            for mtset in mts:
                mtset.updateTotalLength(t)
                rhos += [mtset.getDensityForNuc(t)]
            rho = sum(rhos)
            # determine if nucleation should be from MT
            if not useUnrestrictedNucleation:
                rhohalf = pardict['rhohalfglobal']
                boundfraction = rho/(rho+rhohalf)
            else:
                rn_unbound = pardict['rn_unbound']
                slope = pardict['rn_slope']
                rn_cap = pardict['rn_cap']
                rnglobal = rn_unbound + slope * rho
                boundfraction = 1-rn_unbound/rnglobal
##                print rn_unbound/(pardict['H']*pardict['W']), rnglobal/(pardict['H']*pardict['W'])
                if rnglobal > rn_cap:
                    raise ValueError('Global nucleation rate exceeds cap used to schedule events.')
                if rng.rand() > rnglobal/rn_cap:
                    # discard overbooked nucleations
                    return
            if rng.rand() < boundfraction:
                boundnucleation = True
                # find MT from which to branch
                densityPos = rng.uniform(0,rho)
                if useKataninRelease:
                    if densityPos < rhos[0]:
                        mt = mts[0]
                        totallength = rhos[0]
                    elif densityPos < (rhos[0]+rhos[1]):
                        mt = mts[1]
                        totallength = rhos[1]
                        densityPos -= rhos[0]
                    elif densityPos < (rhos[0]+rhos[1]+rhos[2]):
                        mts = mts[2]
                        totallength = rhos[2]
                        densityPos -= (rhos[0]+rhos[1])
                    else:
                        mts = mts[3]
                        totallength = rhos[3]
                        densityPos -= (rhos[0]+rhos[1]+rhos[2])
                else:
                    if densityPos < rhos[0]:
                        mt = mts[0]
                        totallength = rhos[0]
                    else:
                        mt = mts[1]
                        totallength = rhos[1]
                        densityPos -= rhos[0]
                yposMT = mt.findDensityPos(densityPos,t,totallength = totallength)
                # find ypos for bound nucleation
                nucleationDispersion = pardict['nucleationDispersion']
                ypos = -1
                while ypos < 0 or ypos >= pardict['H']:
                    ypos = rng.normal(yposMT,nucleationDispersion)
                if useRedistributedNucleation:
                    ypos = redistributeNucleationPosition(pardict,ypos)
                    
        elif useNucleationComplexes:
            # find total number of nucleation complexes
            Ncomplexes = len(nucleationcomplexes)
            
            # select random complex for nucleation
            item = rng.randint(0,Ncomplexes)
            nc = nucleationcomplexes.ncs[item]
            boundnucleation = nc.MTbound

            # determine if nucleation should still be executed
            if pardict['rnbound'] > pardict['rn']:
                rnFraction = pardict['rnFraction']
                # skip nucleations for bound NCs to compensate for overbooking
                executeNucleation = False
                if boundnucleation or rng.rand() < rnFraction:
                    executeNucleation = True

            if executeNucleation:
                # obtain ypos
                yposNC = nc.ypos
                if pardict['useMTjumpingNCs']:
                    xpos = nc.xpos

                # find ypos for bound nucleation
                nucleationDispersion = pardict['nucleationDispersion']
                ypos = -1
                while ypos < 0 or ypos >= pardict['H']:
                    ypos = rng.normal(yposNC,nucleationDispersion)

                # remove nucleationcomplex (simplistic approach)
                nc.suicide()

        ####################
        # execute nucleation
        if executeNucleation:
            newDeathEvent = mts[0].nucleate(t,ypos,xpos=xpos)
            # if a new (zero length) MT has a death event, this is a problem
            if newDeathEvent != None:
                raise ValueError('Obtained death event for newly nucleated microtubule')
            
            if (useBoundNucleation or useNucleationComplexes) and boundnucleation:
                eventCounters[4] += 1
            else:
                eventCounters[2] += 1
            
    elif eventType == 'bind':
        if not useNucleationComplexes:
            raise ValueError('Got eventtype "bind" while not using nucleation complexes')

        # Determine position of NC insertion
        executeInsertion = True
        xpos = rng.uniform(0,pardict['W'])
        ypos = rng.uniform(0,pardict['H'])
        
        if useNCattractingMTs:
            executeInsertion = False            
            rbmax = pardict['rbmax']
            rb0 = pardict['rb']
            Kb = pardict['Kb']

            # Find local MT density
            rhoMT = getRhoMT(mts,xpos,ypos,t,pardict)

            # Calculate local insertion rate
            rb = rb0+(rbmax-rb0)*rhoMT/(Kb+rhoMT)

            # Determine if insertion should be executed
            if rng.rand() < rb/rbmax:
                executeInsertion = True

        if useMTdependentInsertion:
            executeInsertion = False

            rb = pardict['rb']
            rbmin = pardict['rbmin']
            # if there is a microtubule within a certain radius, use high insertion rate
            if MTinArea(mts,xpos,ypos,t,pardict):
                executeInsertion = True
            # if not, discard a portion of insertion events
            elif rng.rand() < rbmin/rb:
                executeInsertion = True

        ######################
        # execute NC insertion
        if executeInsertion:
            # bind new NC to the membrane
            nucleationcomplexes.bindToMembrane(xpos = xpos, ypos = ypos)

            # increment counter
            eventCounters[5] += 1

    elif eventType == 'unbind':
        if not useNucleationComplexes:
            raise ValueError('Got eventtype "bind" while not using nucleation complexes')

        # find total number of nucleation complexes
        Ncomplexes = len(nucleationcomplexes)
        
        # select random complex for unbinding
        item = rng.randint(0,Ncomplexes)
        nc = nucleationcomplexes.ncs[item]

        # remove NC
        nc.suicide()

        # increment counter
        eventCounters[6] += 1

    elif eventType == 'hop':
        if not useNucleationComplexes and not pardict['ru'] > 0:
            raise ValueError('Got eventtype "hop" while not using nucleation complexes')

        # detach a random MT-bound NC from its MT
        nucleationcomplexes.detachRandomNC(t, rng)

    else:
        raise ValueError('Unknown eventtype: %s'%str(eventType))
        

def step(mts, tubulinconcentrations, nucleationcomplexes, t, pardict):
    tubulinMode = pardict['tubulinMode']
    useGlobalTubulin = pardict['useGlobalTubulin']
    useKataninRelease = pardict['useKataninRelease']
    trackRegionalDensity = pardict['trackRegionalDensity']
    useNucleationComplexes = pardict['useNucleationComplexes']

    if tubulinMode != 'NoTubulin':
        if useGlobalTubulin:
            # update all lengths using current growth speed
            # without length tracking update of shrinking MTs is also needed!
            for mt in mts:
                mt.updateLengths(t)
            
            # determine new growth speed
            Lt = sum([mt.getTotalStoredLength() for mt in mts])
            vplus0 = pardict['vplus']
            Lmax = pardict['Lmax']
            vplus = vplus0*(1-Lt/Lmax)

            # update growth speeds for regions and sets of growing MTs
            mts[0].updateGlobalVplus(vplus)
            if trackRegionalDensity:
                mts[0].updateRegionalVplus(vplus)
            if useKataninRelease:
                mts[1].updateGlobalVplus(vplus)
                if trackRegionalDensity:
                    mts[1].updateRegionalVplus(vplus)
                    
        elif tubulinMode == 'Local1' or tubulinMode == 'Local2':
            # update all lengths of growing MTs using current growth speed
            mts[0].updateLengths(t)
            if useKataninRelease:
                mts[1].updateLengths(t)

            # update local tubulin concentrations
            tubulinconcentrations.updateConcentrations(t)

            # determine new growth speeds
            A = pardict['H']*pardict['W']
            vplus0 = pardict['vplus']
            Lmax = pardict['Lmax']
            constant = vplus0*A/Lmax
            mts[0].updateRegionalVplus(constant)
            if useKataninRelease:
                mts[1].updateRegionalVplus(constant)
            
        else:
            raise NotImplementedError('Tubulin mode %s not yet implemented'%tubulinMode)

    if useNucleationComplexes:
        # perform diffusive steps on NCs
        nucleationcomplexes.diffusion(t)

def measureRelease(filename,pardict,mts,t,mm, mode = 'a', y = None):
    mtsGn,mtsGtm,mtsSn,mtsStm = mts

    Gnlengths = np.array([mt.length for mt in mtsGn.mts])
    Gtmlengths = np.array([mt.length for mt in mtsGtm.mts])
    Snlengths = np.array([mt.length for mt in mtsSn.mts])
    Stmlengths = np.array([mt.length for mt in mtsStm.mts])
    Gny = np.array([mt.ypos for mt in mtsGn.mts])
    Gtmy = np.array([mt.ypos for mt in mtsGtm.mts])
    Sny = np.array([mt.ypos for mt in mtsSn.mts])
    Stmy = np.array([mt.ypos for mt in mtsStm.mts])
    f = h5py.File(filename + '.hdf5',mode)
    if type(y) != type(None):
        f.create_dataset('y',data = y)
    f.create_dataset('Gnlengths_m' + str(mm),data = Gnlengths)
    f.create_dataset('Gtmlengths_m' + str(mm),data = Gtmlengths)
    f.create_dataset('Snlengths_m' + str(mm),data = Snlengths)
    f.create_dataset('Stmlengths_m' + str(mm),data = Stmlengths)
    f.create_dataset('Gny_m' + str(mm),data = Gny)
    f.create_dataset('Gtmy_m' + str(mm),data = Gtmy)
    f.create_dataset('Sny_m' + str(mm),data = Sny)
    f.create_dataset('Stmy_m' + str(mm),data = Stmy)
    f.close()
    rng = pardict['rng']
    rst = rng.get_state()
    saveRandomState(filename + '.hdf5', rst)

    # some tests
    assert mtsGn.number == len(mtsGn)
    assert mtsGtm.number == len(mtsGtm)
    assert mtsSn.number == len(mtsSn)
    assert mtsStm.number == len(mtsStm)

def measure(filename,pardict,mts,tubulinconcentrations,nucleationcomplexes,t,mm,eventCounters, mode = 'a', y = None):
    print('Measurement at t = {}'.format(t))

    useKataninRelease = pardict['useKataninRelease']
    useBoundNucleation = pardict['useBoundNucleation']
    trackRegionalDensity = pardict['trackRegionalDensity']
    useGlobalTubulin = pardict['useGlobalTubulin']
    useRegionalTubulin = pardict['useRegionalTubulin']
    tubulinMode = pardict['tubulinMode']
    useNucleationComplexes = pardict['useNucleationComplexes']
    useMTjumpingNCs = pardict['useMTjumpingNCs']
    if useKataninRelease:
        measureRelease(filename,pardict,mts,t,mm, mode = mode, y = y)
        Ngrowing = len(mts[0])+len(mts[1])
        Nshrinking = len(mts[2])+len(mts[3])
        Ntm = len(mts[1])+len(mts[3])

    else:   
        # Save current state for simple case
        mtsG,mtsS = mts
        Glengths = np.array([mt.length for mt in mtsG.mts])
        Slengths = np.array([mt.length for mt in mtsS.mts])
        Gy = np.array([mt.ypos for mt in mtsG.mts])
        Sy = np.array([mt.ypos for mt in mtsS.mts])
        f = h5py.File(filename + '.hdf5',mode)
        if type(y) != type(None):
            f.create_dataset('y',data = y)
        f.create_dataset('Glengths_m' + str(mm),data = Glengths)
        f.create_dataset('Slengths_m' + str(mm),data = Slengths)
        f.create_dataset('Gy_m' + str(mm),data = Gy)
        f.create_dataset('Sy_m' + str(mm),data = Sy)
        if useRegionalTubulin:
            # save tubulin concentrations
            f.create_dataset('tubulinconcentration1_m' + str(mm), data = tubulinconcentrations.cT1)
            if tubulinMode == 'Local2':
                f.create_dataset('tubulinconcentration2_m' + str(mm), data = tubulinconcentrations.cT2)
            # save indices of region objects in list of regions for each mt
            Gregionindices = [mt.region.index for mt in mtsG.mts]
            Sregionindices = [mt.region.index for mt in mtsS.mts]
            f.create_dataset('regionindicesG_m' + str(mm), data = Gregionindices)
            f.create_dataset('regionindicesS_m' + str(mm), data = Sregionindices)
        if useNucleationComplexes:
            f.create_dataset('NCy_m' + str(mm),data = np.array([nc.ypos for nc in nucleationcomplexes.ncs]))
            f.create_dataset('NCboundMT_m' + str(mm),data = np.array([nc.MTbound for nc in nucleationcomplexes.ncs]))
        if useMTjumpingNCs:
            Gxpos1s = np.array([mt.xpos1 for mt in mtsG.mts])
            Sxpos1s = np.array([mt.xpos1 for mt in mtsS.mts])
            NCxs = np.array([nc.xpos for nc in nucleationcomplexes.ncs])
            f.create_dataset('Gxpos1_m' + str(mm), data = Gxpos1s)
            f.create_dataset('Sxpos1_m' + str(mm), data = Sxpos1s)
            f.create_dataset('NCx_m' + str(mm), data = NCxs)
        f.create_dataset('eventCounters_m' + str(mm),data = np.array(eventCounters))
        f.close()
        rng = pardict['rng']
        rst = rng.get_state()
        saveRandomState(filename + '.hdf5', rst)

        # some tests
        Ngrowing = len(mtsG)
        Nshrinking = len(mtsS)
        assert mtsG.number == Ngrowing
        assert mtsS.number == Nshrinking
        if trackRegionalDensity:
            if not nearlyEqual(mtsG.density,mtsG.getTotalStoredLength(),epsilonmultiplier=1000000000):
                raise AssertionError('Tracked length of growing MTs does not match total stored length\n%e vs %e'%(mtsG.density,mtsG.getTotalStoredLength()))
            if not nearlyEqual(mtsS.density,mtsS.getTotalStoredLength(),epsilonmultiplier=1000000000):
                raise AssertionError('Tracked length of shrinking MTs does not match total stored length\n%e vs %e'%(mtsS.density,mtsS.getTotalStoredLength()))
            totalLengthRegionsG = sum([region.length for region in mtsG.regions])
            totalLengthRegionsS = sum([region.length for region in mtsS.regions])
            if not nearlyEqual(mtsG.density,totalLengthRegionsG,epsilonmultiplier=1000000000):
                raise AssertionError('Tracked length of growing MTs does not match total tracked length in regions\n%e vs %e'%(mtsG.density,totalLengthRegionsG))
            if not nearlyEqual(mtsS.density,totalLengthRegionsS,epsilonmultiplier=1000000000):
                raise AssertionError('Tracked length of shrinking MTs does not match total tracked length in regions\n%e vs %e'%(mtsS.density,totalLengthRegionsS))
##            for region in mtsG.regions:
##                totLength = sum([mt.length for mt in region.mts])
##                assert nearlyEqual(region.length,totLength,epsilonmultiplier=10000000)
##            for region in mtsS.regions:
##                totLength = sum([mt.length for mt in region.mts])
##                assert nearlyEqual(region.length,totLength,epsilonmultiplier=10000000)
        if tubulinMode == 'Local1':
            A = pardict['H']*pardict['W']
            N = mts[0].Nregions
            Ltot = sum([mt.density for mt in mts])+A/N*sum(tubulinconcentrations.cT1)
            assert abs(pardict['Lmax']-Ltot)/pardict['Lmax']*100 < 0.1
        if tubulinMode == 'Local2':
            A = pardict['H']*pardict['W']
            N = mts[0].Nregions
            Ltot = sum([mt.density for mt in mts])+A/N*sum(tubulinconcentrations.cT1)+A/N*sum(tubulinconcentrations.cT2)
            assert abs(pardict['Lmax']-Ltot)/pardict['Lmax']*100 < 0.1
        if useMTjumpingNCs:
            for mtset in mts:
                for mt in mtset.mts:
                    assert abs((mt.xpos2-mt.xpos1)-mt.length) < 1e-5

    # print stuff
    print( 'Total growing: %d'%Ngrowing)
    print( 'Total shrinking: %d'%Nshrinking)
    if useKataninRelease:
        print( 'Total treadmilling: %d'%Ntm)
    print( 'Total catastrophes: %d'%eventCounters[0])
    print( 'Total rescues: %d'%eventCounters[1])
    print( 'Total nucleations: %d'%(eventCounters[2]+eventCounters[4]))
    if useKataninRelease:
        print( 'Total releases: %d'%eventCounters[3])
    if trackRegionalDensity:
        totalMTlength = sum([mt.density for mt in mts])
    else:
        totalMTlength = 0
        for mtset in mts:
            totalMTlength += sum([mt.length for mt in mtset.mts])
    print( 'Total MT length: %.1f'%(totalMTlength))
    if (useBoundNucleation or useNucleationComplexes) and eventCounters[2]>0:
        print( 'Bound nucleations: %.1f%%'%(eventCounters[4]/(eventCounters[2]+eventCounters[4])*100))
    if useGlobalTubulin:
        print 'Current global vplus: %.03f out of %.03f'%(mts[0].vplus,pardict['vplus'])
    if useRegionalTubulin:
        vplusses = [region.v+pardict['vtm'] for region in mts[0].regions]
        minv = min(vplusses)
        maxv = max(vplusses)
        print 'Regional vplus %.03f-%.03f out of %.03f'%(minv,maxv,pardict['vplus'])
    if useNucleationComplexes:
        print 'Number of nucleation complexes: %d' % len(nucleationcomplexes)
        print 'Number of NC insertions/removals: %d/%d'%(eventCounters[5],eventCounters[6])
        

def run(filename):
    # get parameters
    pardict = read_pars(filename)
    H = pardict['H']
    W = pardict['W']
    rn = pardict['rn']
    tstop = pardict['tstop']
    tmeas = pardict['tmeas']
    randomseed = pardict['randomseed']
    dy = pardict['dy']
    dt = pardict['dt']


    # Prepare variables
    if 'useBands' in pardict and pardict['useBands']:
        useBands = True
    else:
        useBands = False
    pardict['useBands'] = useBands
    
    if 'MTdependentNucleation' in pardict:
        MTdependentNucleation = pardict['MTdependentNucleation']
        if MTdependentNucleation == 'Simple':
            useBoundNucleation = True
            useUnrestrictedNucleation = False
        elif MTdependentNucleation == 'Unrestricted':
            useBoundNucleation = True
            useUnrestrictedNucleation = True
        else:
            useBoundNucleation = False
            useUnrestrictedNucleation = False
        if MTdependentNucleation == 'NucleationComplexes':
            useNucleationComplexes = True
        else:
            useNucleationComplexes = False
    else:
        MTdependentNucleation = 'None'
        pardict['MTdependentNucleation'] = MTdependentNucleation
        useBoundNucleation = False
        useNucleationComplexes = False
    pardict['useBoundNucleation'] = useBoundNucleation
    pardict['useUnrestrictedNucleation'] = useUnrestrictedNucleation
    pardict['useNucleationComplexes'] = useNucleationComplexes

    if 'useKataninRelease' in pardict and pardict['useKataninRelease']:
        useKataninRelease = True
    else:
        useKataninRelease = False
    pardict['useKataninRelease'] = useKataninRelease
        
    if 'useRedistributedNucleation' in pardict and pardict['useRedistributedNucleation']:
        useRedistributedNucleation = True
    else:
        useRedistributedNucleation = False
    pardict['useRedistributedNucleation'] = useRedistributedNucleation
    
    if not 'tubulinMode' in pardict:
        pardict['tubulinMode'] = 'NoTubulin'
    tubulinMode = pardict['tubulinMode']
    if tubulinMode == 'Global':
        useGlobalTubulin = True
    else:
        useGlobalTubulin = False
    if tubulinMode.startswith('Local'):
        useRegionalTubulin = True
    else:
        useRegionalTubulin = False
    pardict['useGlobalTubulin'] = useGlobalTubulin
    pardict['useRegionalTubulin'] = useRegionalTubulin
    
    if useBoundNucleation or useRegionalTubulin or useNucleationComplexes:
        trackRegionalDensity = True
    else:
        trackRegionalDensity = False
    pardict['trackRegionalDensity'] = trackRegionalDensity
    
    if 'useNucleationSaturation' in pardict:
        useNucleationSaturation = pardict['useNucleationSaturation']
    pardict['useNucleationSaturation'] = useNucleationSaturation
    
    if 'rnbound' in pardict:
        if pardict['rnbound'] > pardict['rn']:
            pardict['rnFraction'] = pardict['rn']/pardict['rnbound']
    else:
        pardict['rnbound'] = -1
        
    if 'useMTjumpingNCs' in pardict:
        if pardict['useMTjumpingNCs'] and not pardict['useNucleationComplexes']:
            raise ValueError('Cannot use MT jumping NCs without NCs')
    else:
        pardict['useMTjumpingNCs'] = False
        
    if 'useNCattractingMTs' in pardict:
        if pardict['useNCattractingMTs']:
            raise NotImplementedError('NC attracting MTs not fully implemented')
            if not pardict['useNucleationComplexes']:
                raise ValueError('Cannot use NC attracting MTs without NCs')
    else:
        pardict['useNCattractingMTs'] = False

    if 'useMTdependentInsertion' in pardict:
        if pardict['useMTdependentInsertion']:
            if not pardict['useNucleationComplexes']:
                raise ValueError('Cannot use MT-dependent NC insertion without NCs')
            if not pardict['useMTjumpingNCs']:
                raise ValueError('Cannot use MT-dependent NC insertion without 2D NC implementation')
    else:
        pardict['useMTdependentInsertion'] = False

    if not 'preseeddensity' in pardict:
        pardict['preseeddensity'] = 0

    if 'parameterSwitchTimes' in pardict:
        parameterSwitchTimes = pardict['parameterSwitchTimes']
        useparswitch = True
    else:
        parameterSwitchTimes = None
        useparswitch = False

    if type(pardict['rcat']) == type([]):
        rcatValues = pardict['rcat']
        pardict['rcat'] = rcatValues[0]
        switchrcat = True
    else:
        switchrcat = False
    if type(pardict['rcatGaps']) == type([]):
        rcatGapsValues = pardict['rcatGaps']
        pardict['rcatGaps'] = rcatGapsValues[0]
        switchrcatGap = True
    else:
        switchrcatGap = False
    if type(pardict['rres']) == type([]):
        rresValues = pardict['rres']
        pardict['rres'] = rresValues[0]
        switchrres = True
    else:
        switchrres = False
    if type(pardict['vplus']) == type([]):
        vplusValues = pardict['vplus']
        pardict['vplus'] = vplusValues[0]
        switchvplus = True
    else:
        switchvplus = False
    if type(pardict['vmin']) == type([]):
        vminValues = pardict['vmin']
        pardict['vmin'] = vminValues[0]
        switchvmin = True
    else:
        switchvmin = False

        
    npointsy = int(round(H/dy,0))
    y = np.arange(0,H,dy)
    y = np.zeros((1,npointsy))+y
    rnglobal = rn*H*W
    pardict['rnglobal'] = rnglobal
    if useBands:
        nBands = pardict['nBands']
        bandwidth = pardict['bandwidth']
        totalBandwidth = nBands*bandwidth
        totalGapwidth = H-totalBandwidth
        gapwidth = totalGapwidth/nBands
        period = bandwidth + gapwidth
        bandstartpos = gapwidth/2
        bandendpos = bandstartpos + bandwidth
        pardict['period'] = period
        pardict['bandstartpos'] = bandstartpos
        pardict['bandendpos'] = bandendpos
        pardict['rcatFraction'] = pardict['rcat']/pardict['rcatGaps']
    if useBoundNucleation:
        pardict['rhohalfglobal'] = pardict['rhohalf']*H*W
    if useUnrestrictedNucleation:
        vp = pardict['vplus']
        vm = pardict['vmin']
        vtm = pardict['vtm']
        rc = pardict['rcat']
        rr = pardict['rres']
        Lmt = rnglobal * ((vp+vm)*(vp-vtm)*(vm+vtm))/(rc*(vm+vtm)-rr*(vp-vtm))**2
        slope = (1-pardict['f_unbound']) * rnglobal / Lmt
        rn_unbound = pardict['f_unbound']*rnglobal
        pardict['Lmt_eq'] = Lmt
        pardict['rn_unbound'] = rn_unbound
        pardict['rn_slope'] = slope
        pardict['rn_cap'] = 3*rnglobal

    if tubulinMode == 'Local2' and useKataninRelease:
        raise NotImplementedError('proper growth tracking for katanin system not implemented')

    # Set up random number generator    
    if randomseed == 'random':
        rng = RandomState()
    else:
        rng = RandomState(randomseed)
    pardict['rng'] = rng

    # Set up initial condition
    if useRegionalTubulin:
        Nregions = npointsy
        useSpatialRegions = True
        if tubulinMode.startswith('Local'):
            A = W*H
            Aregion = A/npointsy
            Tregion = pardict['Lmax']/npointsy
            localTconc = pardict['Lmax']/A
            tubulinconcentrations = TubulinConcentrations(pardict,npointsy,localTconc)
        else:
            raise NotImplementedError('tubulinMode %s is not yet implemented'%tubulinMode)
    elif useNucleationSaturation:
        Nregions = npointsy
        useSpatialRegions = True
        tubulinconcentrations = None
        pardict['saturationLength'] = pardict['saturationDensity']*H*W/Nregions
    else:
        Nregions = 100
        useSpatialRegions = False
        tubulinconcentrations = None
    if useNucleationComplexes:
        Nregions = npointsy
        useSpatialRegions = True
    pardict['useSpatialRegions'] = useSpatialRegions
        
    if useKataninRelease:
        mtsGn = MTset(True,pardict,treadmilling = False, Nregions = Nregions, tubulinconcentrations = tubulinconcentrations)
        mtsGtm = MTset(True,pardict,treadmilling = True, Nregions = Nregions, tubulinconcentrations = tubulinconcentrations)
        mtsSn = MTset(False,pardict,treadmilling = False, Nregions = Nregions, tubulinconcentrations = tubulinconcentrations)
        mtsStm = MTset(False,pardict,treadmilling = True, Nregions = Nregions, tubulinconcentrations = tubulinconcentrations)
        mts = [mtsGn,mtsGtm,mtsSn,mtsStm]
    else:
        mtsG = MTset(True,pardict, Nregions = Nregions, tubulinconcentrations = tubulinconcentrations)
        mtsS = MTset(False,pardict, Nregions = Nregions, tubulinconcentrations = tubulinconcentrations)
        mts = [mtsG,mtsS]

    if useRegionalTubulin:
        tubulinconcentrations.linkMTs(mts)

    if useNucleationComplexes:
        nucleationcomplexes = NucleationComplexes(mts,pardict)
    else:
        nucleationcomplexes = None
    

    # Set up queue
    detQ = DeterministicQueue()

    # Set up measure times
    mtimes = list(np.arange(tmeas,tstop+tmeas,tmeas))
    stoptime = mtimes[-1]
    t = 0
    t_list = [t]
    mm = 1

    # Set up parameter switch times
    if useparswitch:
        stimes = parameterSwitchTimes[:]
        nextSwitchTime = stimes[0]
        stimes = stimes[1:]
        if nearlyEqual(tmeas,nextSwitchTime):
            switchParsNextMeas = True
            nextSwitchTime = stimes[0]
            stimes = stimes[1:]
        else:
            switchParsNextMeas = False

    # Store initial conditions
    eventCounters = [0,0,0,0,0,0,0] # cat/res/nucFree/rel/nucBound/NCinsertion/NCremoval
    measure(filename,pardict,mts,tubulinconcentrations,nucleationcomplexes,t,mm,eventCounters, mode = 'w', y = y)
    print ''

    # Preseed array
    preseeddensity = pardict['preseeddensity']
    if preseeddensity > 0:
        nseeds = int(round(preseeddensity*H*W))
        for seed in range(nseeds):
            ypos = rng.uniform(0,H)
            xpos = rng.uniform(0,W)
            mts[0].nucleate(t,ypos,xpos=xpos)

    start_time = time.time()
    tm = tmeas
    if tubulinMode != 'NoTubulin' or useNucleationComplexes:
        tstep = dt
    else:
        tstep = stoptime*2
    scheduledStochasticEvents = 0
    discardedDeterministicEvents = 0
    executedDeterministicEvents = 0
    # Main loop
    while t < (stoptime+0.01*tmeas):
        # Find next stochastic event
        dts,eventType = getTimeInterval(mts,nucleationcomplexes,pardict,rng)
        scheduledStochasticEvents += 1

        # Find next deterministic event
        # pop only invalid events here! (pop valid ones on execution)
        valid = False
        while not valid and len(detQ) > 0:
            valid = detQ.first().valid
            if valid:
                td = detQ.first().eventTime
            else:
                detQ.pop()
                discardedDeterministicEvents += 1
        if len(detQ) == 0:
            td = stoptime + 100 # no deterministic events left

        
        dtm = tm - t
        dtd = td - t
        dtstep = tstep - t
        if min(dtm, dtstep, dts) < 0:
            print t, dtm, dtd, dtstep, dts
            raise ValueError('Attempting to execute event in the past.')
        if dtm < dts and dtm < dtd and dtm < dtstep:
            t += dtm
            tm += tmeas
            if useparswitch:
                if switchParsNextMeas:
                    switchParsNow = True
                    switchParsNextMeas = False
                else:
                    switchParsNow = False
                if nearlyEqual(tm, nextSwitchTime):
                    switchParsNextMeas = True
                    if len(stimes) > 0:
                        nextSwitchTime = stimes[0]
                        stimes = stimes[1:]
                    else:
                        nextSwitchTime = stoptime + tmeas + 100 # no switch times left
                elif tm > nextSwitchTime:
                    tm = nextSwitchTime
                    warnings.warn("Warning: Parameter switch times don't match with measurement times.")
                    switchParsNextMeas = True
                    if len(stimes) > 0:
                        nextSwitchTime = stimes[0]
                        stimes = stimes[1:]
                    else:
                        nextSwitchTime = stoptime + 100 # no switch times left
                else:
                    switchParsNextMeas = False
                    
                    

            # update MTs
            for mt in mts:
                mt.updateLengths(t)
            
            # perform measurement
            mm += 1
            measure(filename,pardict,mts,tubulinconcentrations,nucleationcomplexes,t,mm,eventCounters)
            t_list.append(t)
            
            totalDeterministicEvents = discardedDeterministicEvents + executedDeterministicEvents
            if totalDeterministicEvents > 0:
                print( 'Discarded deterministic events: %.0f%%'%(discardedDeterministicEvents/totalDeterministicEvents*100))
            discardedStochasticEvents = scheduledStochasticEvents - sum(eventCounters)
            print( 'Discarded stochastic events: %.0f%%\n'%(discardedStochasticEvents/scheduledStochasticEvents*100))

            # switch parameters
            if useparswitch and switchParsNow:
                regionUpdate = False
                print( 'Switching parameters\n')
                if switchrcat:
                    rcatValues = rcatValues[1:]
                    pardict['rcat'] = rcatValues[0]
                if switchrcatGap:
                    rcatGapsValues = rcatGapsValues[1:]
                    pardict['rcatGaps'] = rcatGapsValues[0]
                pardict['rcatFraction'] = pardict['rcat']/pardict['rcatGaps']
                if switchrres:
                    rresValues = rresValues[1:]
                    pardict['rres'] = rresValues[0]
                if switchvplus:
                    vplusValues = vplusValues[1:]
                    pardict['vplus'] = vplusValues[0]
                    for mtset in mts:
                        mtset.updateGlobalVplus(pardict['vplus'])
                        if pardict['trackRegionalDensity']:
                            regionUpdate = True
                if switchvmin:
                    vminValues = vminValues[1:]
                    pardict['vmin'] = vminValues[0]
                    if pardict['trackRegionalDensity']:
                        regionUpdate = True
                    # reset times of death events using new vmin
                    detQ.resetDeathTimes(pardict['vmin'],pardict['vtm'],t)

                    # test integrity of the deterministic queue
                    copy = [event for event in detQ.queue]
                    lasteventtime = 0
                    while (len(copy)>0):
                        event = heappop(copy)
                        if event.valid:
                            if event.eventTime < lasteventtime:
                                raise ValueError('incorrect order in detQ')
                            lasteventtime = event.eventTime

                if regionUpdate:
                    for mtset in mts:
                        mtset.updateRegionalNetspeed()
                        regionUpdate = False
                    

        elif dtd < dts and dtd < dtstep:
            t += dtd

            # pop event from queue
            event = detQ.pop()
            if dtd < 0:
                raise ValueError('eventTime is in the past at t'+str(dtd)+'\ntime point before error: '+str(t-dtd)+'.\n Residual associated MT length = '+str(event.mt.length)+'\nLast MT update at t = ' + str(event.mt.lastupdate) + '\nvmin = ' + str(event.mt.owner.pardict['vmin']) + '\nevent time = ' + str(event.eventTime))

            # execute deterministic event
            event.execute(t)
            executedDeterministicEvents += 1

        elif dtstep < dts:
            t += dtstep
            tstep += dt

            # execute integration time step
            step(mts, tubulinconcentrations, nucleationcomplexes, t, pardict)

        else:
            t += dts
            
            # execute stochastic event
            handleEvent(eventType, mts, detQ, nucleationcomplexes, t, pardict,eventCounters)
                

    # Print duration of main loop   
    end_time = time.time()
    print 'Runtime:', end_time-start_time, '\n'
    f = open(filename + '.pars','a')
    f.write('Runtime: ' + str(end_time-start_time) + 's\n')
    f.close()

    # Store measured timepoints
    f = h5py.File(filename + '.hdf5','a')
    if 't' in f:
        del f['t']
    f.create_dataset('t',data = t_list)
    f.close()
        
        

    


def write_pars(filename, H, W, vplus, vmin, vtm, rcat, rres, rn, tstop, tmeas, randomseed = 'random', dy = 0.5, dt = 0.5, rrel = 0, nBands = 0, bandwidth = 1, rcatGaps = 0, rhohalf = 0, nucleationDispersion = 0, tubulinMode = 'NoTubulin', Lmax = 0, Dtub = 0, integrationMethod = 'CrankNicolson', Dtub2 = 0, beta = 0, useNucleationSaturation = False, saturationDensity = 0, rd = 0, rb = 0, Dnc = 0, useMTjumpingNCs = False, rnbound = -1, useNCattractingMTs = False, rbmax = 0, Kb = 0, densityResolution = 0, useBands = False, MTdependentNucleation = 'None', useRedistributedNucleation = False, useKataninRelease = False, parameterSwitchTimes = None, ru = 0, f_unbound = 0, rskip = 0, useMTdependentInsertion = False, rbmin = 0, searchradius = 0, preseeddensity = 0):
    text = ''
    text += 'H ' + str(H) + '\n'
    text += 'W ' + str(W) + '\n'
    if type(vplus) == type([]):
        if tubulinMode != 'NoTubulin':
            raise ValueError('Changing vplus0 for variable vplus not implemented and possibly not meaningful.')
        text += 'vplus'
        for vp in vplus:
            text += ' ' + str(vp)
        text += '\n'
    else:
        text += 'vplus ' + str(vplus) + '\n'
    if type(vmin) == type([]):
        text += 'vmin'
        for vm in vmin:
            text += ' ' + str(vm)
        text += '\n'
    else:
        text += 'vmin ' + str(vmin) + '\n'
    text += 'vtm ' + str(vtm) + '\n'
    if type(rcat) == type([]):
        text += 'rcat'
        for rc in rcat:
            text += ' ' + str(rc)
        text += '\n'
    else:
        text += 'rcat ' + str(rcat) + '\n'
    if type(rres) == type([]):
        text += 'rres'
        for rr in rres:
            text += ' ' + str(rr)
        text += '\n'
    else:
        text += 'rres ' + str(rres) + '\n'
    text += 'rn ' + str(rn) + '\n'
    text += 'f_unbound ' + str(f_unbound) + '\n'
    text += 'useKataninRelease ' + str(useKataninRelease) + '\n'
    text += 'rrel ' + str(rrel) + '\n'
    text += 'useBands ' + str(useBands) + '\n'
    text += 'nBands ' + str(nBands) + '\n'
    text += 'bandwidth ' + str(bandwidth) + '\n'
    if type(rcatGaps) == type([]):
        text += 'rcatGaps'
        for rc in rcatGaps:
            text += ' ' + str(rc)
        text += '\n'
    else:
        text += 'rcatGaps ' + str(rcatGaps) + '\n'
    text += 'MTdependentNucleation ' + str(MTdependentNucleation) + '\n'
    text += 'rhohalf ' + str(rhohalf) + '\n'
    text += 'nucleationDispersion ' + str(nucleationDispersion) + '\n'
    text += 'useRedistributedNucleation ' + str(useRedistributedNucleation) + '\n'
    text += 'tubulinMode ' + str(tubulinMode) + '\n'
    text += 'Lmax ' + str(Lmax) + '\n'
    text += 'Dtub ' + str(Dtub) + '\n'
    text += 'Dtub2 ' + str(Dtub2) + '\n'
    text += 'beta ' + str(beta) + '\n'
    text += 'useNucleationSaturation ' + str(useNucleationSaturation) + '\n'
    text += 'saturationDensity ' + str(saturationDensity) + '\n'
    text += 'useMTjumpingNCs ' + str(useMTjumpingNCs) + '\n'
    text += 'rb ' + str(rb) + '\n'
    text += 'rd ' + str(rd) + '\n'
    text += 'ru ' + str(ru) + '\n'
    text += 'Dnc ' + str(Dnc) + '\n'
    text += 'rnbound ' + str(rnbound) + '\n'
    text += 'rskip ' + str(rskip) + '\n'
    text += 'useNCattractingMTs ' + str(useNCattractingMTs) + '\n'
    text += 'rbmax ' + str(rbmax) + '\n'
    text += 'Kb ' + str(Kb) + '\n'
    text += 'densityResolution ' + str(densityResolution) + '\n'
    text += 'useMTdependentInsertion ' + str(useMTdependentInsertion) + '\n'
    text += 'rbmin ' + str(rbmin) + '\n'
    text += 'searchradius ' + str(searchradius) + '\n'
    text += 'preseeddensity ' + str(preseeddensity) + '\n'
    text += 'integrationMethod ' + str(integrationMethod) + '\n'
    text += 'tstop ' + str(tstop) + '\n'
    text += 'tmeas ' + str(tmeas) + '\n'
    text += 'randomseed ' + str(randomseed) + '\n'
    text += 'dy ' + str(dy) + '\n'
    text += 'dt ' + str(dt) + '\n'
    if parameterSwitchTimes != None:
        text += 'parameterSwitchTimes'
        for time in parameterSwitchTimes:
            text += ' ' + str(time)
        text += '\n'

    f = open(filename + '.pars','w')
    f.write(text)
    f.close()
    

def read_pars(filename):
    names =  ('H','W','vplus','vmin','vtm','rcat','rres','rn','tstop','tmeas','randomseed','dy','dt','rrel','nBands','bandwidth','rcatGaps','rhohalf','nucleationDispersion','tubulinMode','Lmax','Dtub','integrationMethod','Dtub2','beta','useNucleationSaturation','saturationDensity','useNucleationComplexes','Dnc','rb','rd','useMTjumpingNCs','rnbound','useNCattractingMTs','rbmax','Kb','densityResolution','useBands','MTdependentNucleation','useRedistributedNucleation','useKataninRelease','parameterSwitchTimes','ru','f_unbound','rskip','useMTdependentInsertion','rbmin','searchradius','preseeddensity')
    d = {}
    f = open(filename + '.pars')
    for line in f:
        parts = line.split()
        if parts == []:
            pass
        elif parts[0] in names:
            if parts[0] in ('tubulinMode','integrationMethod','MTdependentNucleation'):
                d[parts[0]] = parts[1]
            elif parts[0] in ('randomseed','nBands'):
                if parts[1] == 'random':
                    d[parts[0]] = 'random'
                else:
                    d[parts[0]] = int(parts[1])
            elif parts[0].startswith('use'):
                d[parts[0]] = eval(parts[1])
            elif (parts[0] in ('rcat','rcatGaps','rres','vmin','vplus') and len(parts) > 2) or parts[0] == 'parameterSwitchTimes':
                l = []
                for ii in range(1,len(parts)):
                    l += [float(parts[ii])]
                d[parts[0]] = l
            else:
                d[parts[0]] = float(parts[1])
    f.close()
    return d

