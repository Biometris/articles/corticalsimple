#!/usr/bin/python

# Example simulation run script from FigS4.8

from __future__ import division
import sys
import os
import numpy as np
from numpy.random import RandomState
from CorticalSimple import write_pars, run
from processOutput import plot_Tend, movie_solution, plotSummaryQuantities, plot_movingAverage


if __name__ == '__main__':
    useBands = True
    MTdependentNucleation = 'NucleationComplexes' #None/Simple/NucleationComplexes
    useRedistributedNucleation = False
    tubulinMode = 'NoTubulin' #NoTubulin/Global/Local1/Local2
    useMTjumpingNCs = True # Allow NCs to move around MTs
    useMTdependentInsertion = True

    # run setup
    randomseedBase = 71 # Base for random seed of this script
    reps = range(100) # Run for 100 replicates

    # check for command line arguments
    args = sys.argv
    if len(args) > 2:
        singlerun = True
        parallelreps = True
        rep = int(args[1])
        ii = int(args[2])
    elif len(args) > 1:
        singlerun = False
        parallelreps = True
        rep = int(args[1])
    else:
        singlerun = False
        parallelreps = False

    # system parameters
    H = 60 # Domain height (um)
    W = 7.5*2*np.pi # Domain width (um)
    tstop = 7*3600 # Simulation end time (s)
    tmeas = 200 # Measurement time (s)
    parameterSwitchTimes = [7200] # Time where bandformation starts (s)

    # basic microtubule dynamics parameters
    vplus = 0.05 # MT growth speed (um/s)
    vmin = 0.08 # MT shrink speed (um/s)
    vtm = 0.01 # MT minus end retraction speed (um/s)
    rres = 0.001 # Rescue rate (events/shrinkingMT/s)
    rcat = 0.0016 # Catastrophe rate (events/growingMT/s)
##    rn = 0.001 # Nucleation rate (events/um^2)

    # discretisation parameters (depending on system)
    dy = 0.2 # Region size for NC efficiency or spatial discretisation for tubulin diffusion (um)
    dt = 0.01 # Time step for NC or tubulin diffusion (s)

    # Band parameters
    nBands = 10 # Number of bands
    bandwidth = 1 # um
##    rcatGaps = 3*rcat # catastrophe rate in gap regions (events/growingMT/s)

    # Simple MT-dependent nucleation parameters (Not used)
    rhohalf=0;nucleationDispersion=0
##    rhohalf = 0.1 # um MT/ um^2
##    nucleationDispersion = 0.1 # um

    # Nucleation complex parameters
##    rb=0;rd=0;Dnc=0;rnbound=0
    rb = 0.0045 # NC insertion rate (insertions/um^2/s)
    rd = 0.1 # NC dissociation rate (events/particle/s)
    Dnc = 0.013 # NC diffusion coefficient (um^2/s)
    nucleationDispersion = 0.1 # Standard deviation on offset newly nucleated mucrotubules (um)
    rnbound = 0.03 # Nucleation rate of MT-bound NCs (events/particle/s)
##    rn = 0.002 # Nucleation rate of non-MT-bound NCs (events/particle/s)
    preseeddensity = 1 # Density of seeded nucleations (nucleations/um^2)
##    rbmin = rb/40 # NC insertion rate when no MTs in radius R (insertions/um^2/s)
    R = 0.05 # If no MTs in radius R, use rbmin instead of rb (um)

    # Tubulin parameters (Not used)
    Lmax=0;Dtub=0;Dtub2=0;beta=0
    method = 'CrankNicolson' # CrankNicolson/FTCS
##    Lmax = 10*H*W # Total tubulin availability (um MT equivalents)
##    Dtub = 1 # GTP-tubulin diffusion coefficient (um^2/s)
##    Dtub2= Dtub # GDP-tubulin diffusion coefficient (um^2/s)
##    beta = 0.1 # Tubulin recharge rate (1/s)

    # variable parameters
    catFactor = 3 # Factor difference in catastrophe rate bands and gaps
    frbs = [1,10,100] # Factor difference in NC insertion rate in presence vs absence of MTs in radius R
    frns = [100,10,1] # Factor difference nucleation rate MT-bound and non-MT-bound NCs
    iis = range(len(frbs))
    if singlerun:
        iis = [ii]
    if parallelreps:
        reps = [rep]

    baseDir = '../BandsNCrbrn/' # Folder for storing output data
    if not os.path.exists(baseDir):
        os.mkdir(baseDir)
    if not os.path.exists(baseDir + 'Figures/'):
        os.mkdir(baseDir + 'Figures/')
    if not os.path.exists(baseDir + 'BandsNC/'):
        os.mkdir(baseDir + 'BandsNC/')
    basename1 = baseDir + 'BandsNC/frb'
    combifilenames = []
    for ii in iis: # Loop over frbs and frns
        frb = frbs[ii]
        frn = frns[ii]
        rbmin = rb/frb
        rn = rnbound/frn
        rcatGaps = catFactor * rcat
        rcatGaps = [rcat, rcatGaps] # Gap catastrophe rates in startup phase and during band formation
        
        folder1 = basename1 + str(frb) + '_frn' + str(frn) + '/'
        if not os.path.exists(folder1):
            os.mkdir(folder1)
        basename = folder1 + 'sim'

        filenames = []
        for rep in reps:
            randomseed = int('%d%02d%03d'%(randomseedBase,ii,rep))# Individual random seed for each variant and rep
            filename = basename + str(rep+1)

            if not os.path.isfile(filename+'.pars'):
                # Write a parameter file
                write_pars(filename, H, W, vplus, vmin, vtm, rcat, rres, rn, tstop, tmeas, randomseed = randomseed, dy = dy, dt = dt, nBands = nBands, bandwidth = bandwidth, rcatGaps = rcatGaps, rb = rb, rd = rd, Dnc = Dnc, nucleationDispersion = nucleationDispersion, useMTjumpingNCs = useMTjumpingNCs, rnbound = rnbound, useBands = useBands, MTdependentNucleation = MTdependentNucleation, useRedistributedNucleation = useRedistributedNucleation, tubulinMode = tubulinMode, rhohalf = rhohalf, Lmax = Lmax, Dtub = Dtub, Dtub2 = Dtub2, beta = beta, parameterSwitchTimes = parameterSwitchTimes, preseeddensity = preseeddensity, useMTdependentInsertion = useMTdependentInsertion, rbmin = rbmin, searchradius = R)
            if not os.path.isfile(filename+'.hdf5'):
                # Run the simulation
                run(filename)

            filenames += [filename]
            if rep == 00:
##                movie_solution(filename, plotNCs = True)
        
                frame = 'last'
                outname = basename
                plotNCs = False
                plotMTdensityHistogram = False
                plotMovingAvg = True
                window = 3*18
                legend = False
                # Plot MT array at end time
                plot_Tend(filenames, fontsize = 50, ticks = True, outname = outname, printTend = False, caption = None, cblabel = None, ntickscb = None, notext = False, textAbove = False, axlabel = True, frame = frame, rotateLabels = False, plotNCs = plotNCs, make_histogram = False, plotMTdensityHistogram = plotMTdensityHistogram, plotMovingAvg = plotMovingAvg, window = window, legend = legend)
                # Plot time-averaged MT density histogram
                plot_movingAverage(filename, window = window, plotMTdensityHistogram = plotMTdensityHistogram)
                
        combifilenames += [filenames]

    # Plot graphs of summary quantities:
    burntime = 5*3600
    outname = baseDir + 'Figures/combi'
    labels = ['I','II','III']
    t0 = 2*3600
    labelQuantity = 'Scenario'
    labelUnit = None
    plotMTdensityHistogram = False
    window = 3*18
    scaleBoxplots = False
    plotLegends = False
    plotSummaryQuantities(combifilenames, labels = labels, outname = outname, burntime = burntime, labelQuantity = labelQuantity, labelUnit = labelUnit, t0 = t0, plotMTdensityHistogram = plotMTdensityHistogram, window = window, scaleBoxplots = scaleBoxplots, plotLegends = plotLegends)
        
